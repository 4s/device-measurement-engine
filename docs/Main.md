# Device & Measurement Engine

This page will go more into details regarding the purpose, current state, use cases etc. of the Device & Measurement Engine

[TOC]

## Purpose

This library was created to make it easier for frontend developers to create, manipulate and interact with FHIR Device
and Observation resources.
The library allows for creating and manipulating devices and observations through "Builders" that allow for appending
and removing data to a FHIR resource through the use of documented method-calls. Furthermore, the logic inherent in the
FHIR infrastructure has been build into the builders such that the final product will be valid FHIR resources.

Furthermore, this library contains functionality supporting communication with external devices through the 4S
architecture. This functionality could be moved to a separate library for consistency.

## Documentation

This page along with the README.md function as documentation for the library as a whole. Along with this documentation,
an HTML-page containing documentation for all of the methods available in the library can be found in the docs/html
folder of this library. To use it, open the docs/html/index.html page in a browser.

## Maturity level

This library is at a prototype maturity level. All the functionality has been implemented, but there are no guarantees
in relation to the use of this library.

## Current state

At the time of writing, this library has implemented all functionality required in relation to the GMK project.
Some of the fields that are available in the FHIR standard, have not been implemented into the builders yet.
Furthermore, the Device resource is not yet fully implemented in this library. The result of all use of the
DeviceBuilder should be reviewed. Furthermore the functionality regarding communication with external devices through
the 4S architecture may be changed.

## Usage

The main class of this library is DeviceMeasurementEngine.

This section will present som examples of use for this library

**Creating an observation manually**

```typescript
import DeviceMeasurementEngine from 'device-measurement-engine'

const dme = new DeviceMeasurementEngine()
const observation = dme
  .getObservationBuilder()
  .addIdentifier(
    dme
      .getIdentifierBuilder()
      .setSystem('some-system')
      .setValue('some-value')
  )
  .setValueQuantity(80, null, 'kg')
  .build()
```

**Creating a device manually**

```typescript
import DeviceMeasurementEngine from 'device-measurement-engine'

const dme = new DeviceMeasurementEngine()
const observation = dme
  .getDeviceBuilder()
  .addIdentifier(
    dme
      .getIdentifierBuilder()
      .setUse('official')
      .setSystem('some-system')
      .setValue('some-value')
      .build()
  )
  .setManufacturer('some manufacturer')
  .setModelNumber('some model number')
  .build()
```

**Start measurement on the first available device found using FHIR DeviceDefinition**

```typescript
import DeviceMeasurementEngine from 'device-measurement-engine'

const deviceDefinition
const dme = new DeviceMeasurementEngine()
dme.getAvailableDevices(deviceDefinition).then(deviceConnectors => {
  if (deviceConnectors.length > 0) {
    deviceConnectors[0].startMeasurement()
  }
})
```

For more examples of use see the fhir-engine (https://bitbucket.org/4s/fhir-engine.git)

## License

The DeviceMeasurementEngine source code is licensed under the Apache 2.0 license
(https://www.apache.org/licenses/LICENSE-2.0).
