import { CodeableConcept, code, uri } from 'fhir';
/** A class representing a FHIR CodeableConceptBuilder */
export declare class CodeableConceptBuilder {
    /**
     * The CodeableConcept being built
     * @private
     * @type { CodeableConcept }
     * @ignore
     */
    private codeableConcept;
    /**
     * Creates a new CodeableConceptBuilder
     */
    constructor();
    /**
     * Sets the plain text representation of the concept
     * @param { string } text - The plain text representation of the concept
     * @returns { CodeableConceptBuilder } - This CodeableConceptBuilder
     */
    setText(text: string): CodeableConceptBuilder;
    /**
     * Removes the plain text representation of the concept
     * @returns { boolean } A boolean indicating whether the operation was successful
     */
    removeText(): boolean;
    /**
     * Adds a coding to the CodeableConcept
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     */
    addCoding(system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): CodeableConceptBuilder;
    /**
     * Removes a coding specified by the system-property, if one exists
     * @param { uri } system - the system property
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeCoding(system: uri): boolean;
    /**
     * Creates a new CodeableConcept
     * @returns { CodeableConcept } The codeableConcept
     */
    private make;
    /**
     * Builds and returns the CodeableConcept
     * @returns { CodeableConcept } - the CodeableConcept
     */
    build(): CodeableConcept;
}
