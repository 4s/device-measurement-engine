import { CodeableConcept, Coding, code, uri } from 'fhir'

/** A class representing a FHIR CodeableConceptBuilder */
export class CodeableConceptBuilder {
  /**
   * The CodeableConcept being built
   * @private
   * @type { CodeableConcept }
   * @ignore
   */
  private codeableConcept: CodeableConcept

  /**
   * Creates a new CodeableConceptBuilder
   */
  public constructor() {
    this.codeableConcept = this.make()
  }

  /**
   * Sets the plain text representation of the concept
   * @param { string } text - The plain text representation of the concept
   * @returns { CodeableConceptBuilder } - This CodeableConceptBuilder
   */
  public setText(text: string): CodeableConceptBuilder {
    this.codeableConcept.text = text
    return this
  }

  /**
   * Removes the plain text representation of the concept
   * @returns { boolean } A boolean indicating whether the operation was successful
   */
  public removeText(): boolean {
    delete this.codeableConcept.text
    return true
  }

  /**
   * Adds a coding to the CodeableConcept
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   */
  public addCoding(
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): CodeableConceptBuilder {
    if (!Array.isArray(this.codeableConcept.coding)) {
      this.codeableConcept.coding = []
    }
    else {
      this.codeableConcept.coding.forEach(coding => {
        if (coding.system === system) {
          throw new Error(
            'An coding in that system already exists on the CodeableConcept.'
          )
        }
      })
    }
    const coding: Coding = {
      system,
      version,
      code,
      display,
      userSelected,
    } as Coding

    this.codeableConcept.coding.push(coding)
    return this
  }

  /**
   * Removes a coding specified by the system-property, if one exists
   * @param { uri } system - the system property
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeCoding(system: uri): boolean {
    this.codeableConcept.coding.forEach((coding, index) => {
      if (coding.system === system) {
        this.codeableConcept.coding.splice(index, 1)
        return
      }
    })
    return true
  }

  /**
   * Creates a new CodeableConcept
   * @returns { CodeableConcept } The codeableConcept
   */
  private make(): CodeableConcept {
    const codeableConcept: CodeableConcept = {} as CodeableConcept
    return codeableConcept
  }

  /**
   * Builds and returns the CodeableConcept
   * @returns { CodeableConcept } - the CodeableConcept
   */
  public build(): CodeableConcept {
    return this.codeableConcept
  }
}
