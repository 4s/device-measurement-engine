import { Observation, Device } from 'fhir'
import { Baseplate } from 'phg-js-baseplate'
import { Consumer } from './consumer'

/** An interface for ConsumerBuilders */
export interface ConsumerBuilder {
  /**
   * Sets the baseplate to be used when constructing the Consumer
   * @param baseplate 
   */
  withBaseplate(baseplate: Baseplate): this
  /**
   * Sets the Device MDCCode from which to subscribe to messages
   * @param MDCCode 
   */
  withSubscription(MDCCode: string): this
  /**
   * Sets a callback for handling incoming messages
   * @param callback 
   */
  withCallback(callback: (observation: Observation, device: Device) => void): this
  /**
   * Constructs the Consumer
   */
  create(): Consumer
}



