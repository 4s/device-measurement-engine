/** An interface for  */
export interface Consumer {
  /**
   * Starts the Consumers subscription
   */
  start(): void
  /**
   * Stops the Consumers subscription
   */
  stop(): void
}