import { InterfaceEndpoint, MetaData, Baseplate } from 'phg-js-baseplate';
import { MeasurementReceived } from 'phg-messages/types/FHIRMeasurement_pb';
import { Observation, Device } from "fhir";
import { ConsumerBuilder } from "./consumer-builder";
export declare class PHGCoreConsumerBuilder implements ConsumerBuilder {
    private moduleBase;
    private mdcCode;
    private callback;
    withBaseplate(baseplate: Baseplate): this;
    withSubscription(mdcCode: string): this;
    withCallback(callback: (observation: Observation, device: Device) => void): this;
    create(): InstanceType<typeof PHGCoreConsumerBuilder.PHGCoreConsumer>;
    protected static PHGCoreConsumer: {
        new (moduleBase: any, mdcCode: string, callback: (observation: Observation, device: Device) => void): {
            moduleBase: any;
            mdcCode: string;
            phgConsumer: InterfaceEndpoint;
            callback: (observation: Observation, device: Device) => void;
            start(): void;
            /**
             * LOE needs a signal to start subscribing to a device type. This method
             * sends that event.
             */
            startSubscription(): void;
            stop(): void;
            measurementReceived(measurement: MeasurementReceived, meta: MetaData): void;
        };
    };
}
