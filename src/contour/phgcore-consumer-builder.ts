import { InterfaceEndpoint, MetaData, Baseplate } from 'phg-js-baseplate'
import { MeasurementReceived } from 'phg-messages/types/FHIRMeasurement_pb'
import { SubscribeDeviceType } from 'phg-messages/types/PersonalHealthDevice_pb'
import { Consumer } from "./consumer";
import { Observation, Device } from "fhir";
import { ConsumerBuilder } from "./consumer-builder";

export class PHGCoreConsumerBuilder implements ConsumerBuilder {

  private moduleBase: any
  private mdcCode: string
  private callback: (observation: Observation, device: Device) => void

  public withBaseplate(baseplate: Baseplate): this {
    this.moduleBase = new baseplate.ModuleBase("something")
    return this
  }

  public withSubscription(mdcCode: string): this {
    this.mdcCode = mdcCode
    return this
  }

  public withCallback(callback: (observation: Observation, device: Device) => void): this {
    this.callback = callback
    return this
  }

  public create(): InstanceType<typeof PHGCoreConsumerBuilder.PHGCoreConsumer> {
    return new PHGCoreConsumerBuilder.PHGCoreConsumer(
      this.moduleBase,
      this.mdcCode,
      this.callback
    )
  }

  /* tslint:disable */
  protected static PHGCoreConsumer = class implements Consumer {
    moduleBase: any
    mdcCode: string
    phgConsumer: InterfaceEndpoint
    callback: (observation: Observation, device: Device) => void

    constructor(
      moduleBase: any,
      mdcCode: string,
      callback: (observation: Observation, device: Device) => void
    ) {
      this.moduleBase = moduleBase
      this.mdcCode = mdcCode
      this.callback = callback
      try {
        this.phgConsumer = moduleBase.implementsConsumerInterface("PersonalHealthDevice")
        const measurementConsumer: InterfaceEndpoint = moduleBase.implementsConsumerInterface("FHIRMeasurement")
        measurementConsumer.addEventHandler("MeasurementReceived",
          (input: MeasurementReceived, meta: MetaData) => {
            this.measurementReceived(input, meta)
          }
        )
      } catch (e) {
        throw new Error('error while initiating measurementConsumer: ' + e)
      }
    }

    start() {
      this.moduleBase.start()
      // The subscrition event is scheduled in 3 seconds because the
      // modules on the bp needs to have been started.
      setTimeout(() => this.startSubscription(), 3000)
    }

    /**
     * LOE needs a signal to start subscribing to a device type. This method
     * sends that event.
     */
    startSubscription(): void {
      this.phgConsumer.postEvent("SubscribeDeviceType", (args: SubscribeDeviceType) => { args.setMdcCode(parseInt(this.mdcCode)) })
    }

    stop() {
      this.moduleBase.stop()
    }

    measurementReceived(measurement: MeasurementReceived, meta: MetaData) {
      const mdc = measurement.getDeviceType()
      const observationString = measurement.getFhirObservation()
      const deviceString = measurement.getFhirDevice()

      if (mdc !== this.mdcCode) {
        return
      }
      if (typeof observationString !== "string" || typeof deviceString !== "string") {
        throw new Error('Malformed json')
      }
      try {
        const observation = JSON.parse(observationString)
        const device = JSON.parse(deviceString)
        this.callback(observation, device)
      }
      catch (error) {
        throw error
      }
    }
  }
}
