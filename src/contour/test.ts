import { PHGCoreConsumerBuilder } from "./phgcore-consumer-builder"
import { Baseplate } from "phg-js-baseplate"
import { Observation, Device } from "fhir"

/**
 *  example of using a ConsumerBuilder
 */
export const test = () => {
  const consumer = new PHGCoreConsumerBuilder()
    .withBaseplate({} as Baseplate)
    .withSubscription('823148')
    .withCallback((observation: Observation, device: Device) => {
      // tslint:disable 
      console.log(observation)
      console.log(device)
      // tslint:enable
    })
    .create()
  consumer.start()
  // ...
  consumer.stop()
}