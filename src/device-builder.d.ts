import { Device, code, CodeableConcept, Identifier, uri, DeviceProperty } from 'fhir';
/** Class representing a FHIR Device builder */
export declare class DeviceBuilder {
    /**
     * The Device that is being built
     * @private
     * @type { Device }
     * @ignore
     */
    protected device: Device;
    /**
     * Creates a new FHIR Device
     */
    constructor();
    /**
     * Returns all identifiers for the device
     * @returns { Identifier[] } - the identifiers
     */
    getIdentifiers(): Identifier[];
    /**
     * Returns identifier from specific system if one exists
     * @param { uri } system - the system from which to return the identifier
     * @returns { Identifier } the identifier
     */
    getIdentifier(system: uri): Identifier;
    /**
     * Replaces the identifier array with a new array containing a single identifier
     * @param { Identifier } identifier - the identifier to be set
     * @returns { DeviceBuilder } - this DeviceBuilder
     */
    setIdentifier(identifier: Identifier): DeviceBuilder;
    /**
     * Appends an identifier to the device, if an identifier within the system does not already exist
     * @param { Identifier } identifier - the identifier to be appended
     * @returns { DeviceBuilder } - this DeviceBuilder
     */
    addIdentifier(identifier: Identifier): DeviceBuilder;
    /**
     * Removes an identifier specified by the system-property, if one exists
     * @param { uri } system - the system property
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeIdentifier(system: uri): boolean;
    /**
     * Sets the definition for the device
     * @param { string } reference - literal reference, Relative, internal or absolute URL
     * @param { Identifier } identifier - logical reference, when literal reference is not known
     * @param { string } display - text alternative for the resource
     * @returns { DeviceBuilder } - this DeviceBuilder
     */
    setDefinition(reference?: string, identifier?: Identifier, display?: string): DeviceBuilder;
    /**
     * Removes the definition from the device
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeDefinition(): boolean;
    /**
     * Sets the status of the Device availability as one of the following options:
     * active | inactive | entered-in-error | unknown
     * @param { code } code - The status of the device
     * @returns { DeviceBuilder } - This DeviceBuilder
     */
    setStatus(code: code): DeviceBuilder;
    /**
     * Removes the status from the device
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeStatus(): boolean;
    /**
     * Sets the code for the type of device
     * @param { CodeableConcept } codeableConcept - The code for the type of device
     * @returns { DeviceBuilder } - this DeviceBuilder
     */
    setType(codeableConcept: CodeableConcept): DeviceBuilder;
    /**
     * Removes the type from the device
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeType(): boolean;
    /**
     * Sets the name of the manufacturer of the Device as a string
     * @param { manufacturer } string - The name of the manufacturer
     * @returns { DeviceBuilder } - This DeviceBuilder
     */
    setManufacturer(manufacturer: string): DeviceBuilder;
    /**
     * Removes the name of the manufacturer from the device
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeManufacturer(): boolean;
    /**
     * Sets the model number for the device
     * @param { modelNumber } string - The model number for the device
     * @returns { DeviceBuilder } - This DeviceBuilder
     */
    setModelNumber(modelNumber: string): DeviceBuilder;
    /**
     * Removes the model number for the device
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeModelNumber(): boolean;
    /**
     * Adds a configuration setting to the device e.g., regulation status, time properties
     * @param { DeviceProperty } deviceProperty - The DeviceProperty to add
     * @returns { DeviceBuilder } This DeviceBuilder
     */
    addProperty(deviceProperty: DeviceProperty): DeviceBuilder;
    /**
     * Remove a property for the device based on a system and a code for the type of that property
     * @param { uri } system - the system property
     * @param { code } code - Symbol in syntax defined by the system
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeProperty(system: uri, code: code): boolean;
    /**
     * Adds a specialization to the device i.e. the capabilities supported on a device
     * @param { CodeableConcept } systemType  - The code for standard used to communicate
     * @param { string } version - The version of the standard used to communicate
     * @returns { DeviceBuilder } This DeviceBuilder
     */
    addSpecialization(systemType: CodeableConcept, version: string): DeviceBuilder;
    /**
     * Remove a specialization for the device based on a system and a code for the type of that specialization
     * @param { uri } system - the system specialization
     * @param { code } code - Symbol in syntax defined by the system
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeSpecialization(system: uri, code: code): boolean;
    /**
     * Adds a version of the design of the device or software running on the device, to the device
     * @param { CodeableConcept } type - The type of the device version
     * @param { Identifier } component - A single component of the device version
     * @param { value } string - The version text
     * @returns { DeviceBuilder } This DeviceBuilder
     */
    addVersion(type: CodeableConcept, component: Identifier, value: string): DeviceBuilder;
    /**
     * Removes the version from the device specified by a system and code for the type of version
     * @param { uri } system - the system property
     * @param { code } code - Symbol in syntax defined by the system
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeVersion(system: uri, code: code): boolean;
    /**
     * Make a new object that implements a FHIR Device
     * @returns { Device } - an object that implements FHIR Device
     */
    private make;
    /**
     * Builds and returns the device
     * @returns { Device } - the device
     */
    build(): Device;
}
