import {
  Device,
  code,
  CodeableConcept,
  Identifier,
  uri,
  Reference,
  DeviceProperty,
  DeviceVersion,
  DeviceSpecialization
} from 'fhir'
import { Guid } from './helpers/guid'

/** Class representing a FHIR Device builder */
export class DeviceBuilder {
  /**
   * The Device that is being built
   * @private
   * @type { Device }
   * @ignore
   */
  protected device: Device

  /**
   * Creates a new FHIR Device
   */
  public constructor() {
    this.device = this.make()
  }

  /**
   * Returns all identifiers for the device
   * @returns { Identifier[] } - the identifiers
   */
  public getIdentifiers(): Identifier[] {
    if (Array.isArray(this.device.identifier)) {
      return this.device.identifier
    }
    return []
  }

  /**
   * Returns identifier from specific system if one exists
   * @param { uri } system - the system from which to return the identifier
   * @returns { Identifier } the identifier
   */
  public getIdentifier(system: uri): Identifier {
    let returnIdentifier: Identifier
    this.device.identifier.forEach(identifier => {
      if (identifier.system === system) {
        returnIdentifier = identifier
        return
      }
    })
    return returnIdentifier
  }

  /**
   * Replaces the identifier array with a new array containing a single identifier
   * @param { Identifier } identifier - the identifier to be set
   * @returns { DeviceBuilder } - this DeviceBuilder
   */
  public setIdentifier(identifier: Identifier): DeviceBuilder {
    this.device.identifier = [identifier]
    return this
  }

  /**
   * Appends an identifier to the device, if an identifier within the system does not already exist
   * @param { Identifier } identifier - the identifier to be appended
   * @returns { DeviceBuilder } - this DeviceBuilder
   */
  public addIdentifier(identifier: Identifier): DeviceBuilder {
    if (Array.isArray(this.device.identifier)) {
      if (typeof this.getIdentifier(identifier.system) === 'undefined') {
        this.device.identifier.push(identifier)
      } else {
        throw new Error(
          'An identifier in that system already exists on the device.'
        )
      }
    } else {
      this.device.identifier = [identifier]
    }
    return this
  }

  /**
   * Removes an identifier specified by the system-property, if one exists
   * @param { uri } system - the system property
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeIdentifier(system: uri): boolean {
    this.device.identifier.forEach((identifier, index) => {
      if (identifier.system === system) {
        this.device.identifier.splice(index, 1)
        return
      }
    })
    return true
  }

  /**
   * Sets the definition for the device
   * @param { string } reference - literal reference, Relative, internal or absolute URL
   * @param { Identifier } identifier - logical reference, when literal reference is not known
   * @param { string } display - text alternative for the resource
   * @returns { DeviceBuilder } - this DeviceBuilder
   */
  public setDefinition(
    reference?: string,
    identifier?: Identifier,
    display?: string
  ): DeviceBuilder {
    const definition: Reference = {
      reference,
      identifier,
      display,
    } as Reference
    this.device.definition = definition
    return this
  }

  /**
   * Removes the definition from the device
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeDefinition(): boolean {
    delete this.device.definition
    return true
  }

  /**
   * Sets the status of the Device availability as one of the following options:
   * active | inactive | entered-in-error | unknown
   * @param { code } code - The status of the device
   * @returns { DeviceBuilder } - This DeviceBuilder
   */
  public setStatus(code: code): DeviceBuilder {
    if (
      code !== 'active' &&
      code !== 'inactive' &&
      code !== 'entered-in-error' &&
      code !== 'unknown'
    ) {
      throw new Error(
        'The provided code is not part of the valid set of device status codes'
      )
    }
    this.device.status = code
    return this
  }

  /**
   * Removes the status from the device
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeStatus(): boolean {
    delete this.device.status
    return true
  }

  /**
   * Sets the code for the type of device
   * @param { CodeableConcept } codeableConcept - The code for the type of device
   * @returns { DeviceBuilder } - this DeviceBuilder
   */
  public setType(codeableConcept: CodeableConcept): DeviceBuilder {
    this.device.type = codeableConcept
    return this
  }

  /**
   * Removes the type from the device
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeType(): boolean {
    delete this.device.type
    return true
  }

  /**
   * Sets the name of the manufacturer of the Device as a string
   * @param { manufacturer } string - The name of the manufacturer
   * @returns { DeviceBuilder } - This DeviceBuilder
   */
  public setManufacturer(manufacturer: string): DeviceBuilder {
    this.device.manufacturer = manufacturer
    return this
  }

  /**
   * Removes the name of the manufacturer from the device
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeManufacturer(): boolean {
    delete this.device.manufacturer
    return true
  }

  /**
   * Sets the model number for the device
   * @param { modelNumber } string - The model number for the device
   * @returns { DeviceBuilder } - This DeviceBuilder
   */
  public setModelNumber(modelNumber: string): DeviceBuilder {
    this.device.modelNumber = modelNumber
    return this
  }

  /**
   * Removes the model number for the device
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeModelNumber(): boolean {
    delete this.device.modelNumber
    return true
  }

  /**
   * Adds a configuration setting to the device e.g., regulation status, time properties
   * @param { DeviceProperty } deviceProperty - The DeviceProperty to add
   * @returns { DeviceBuilder } This DeviceBuilder
   */
  public addProperty(deviceProperty: DeviceProperty): DeviceBuilder {
    if (!Array.isArray(this.device.property)) {
      this.device.property = []
    }
    else {
      this.device.property.forEach(property => {
        property.type.coding.forEach(coding => {
          deviceProperty.type.coding.forEach(devicePropertyCoding => {
            if (
              coding.system === devicePropertyCoding.system &&
              coding.code === devicePropertyCoding.code
            ) {
              throw new Error(
                'A property with a type with the same system and coding already exists on the device.'
              )
            }
          })
        })
      })
    }
    this.device.property.push(deviceProperty)
    return this
  }

  /**
   * Remove a property for the device based on a system and a code for the type of that property
   * @param { uri } system - the system property
   * @param { code } code - Symbol in syntax defined by the system
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeProperty(system: uri, code: code): boolean {
    this.device.property.forEach((property, index) => {
      property.type.coding.forEach(coding => {
        if (coding.system === system && coding.code === code) {
          this.device.property.splice(index, 1)
          return
        }
      })
    })
    return true
  }

  /**
   * Adds a specialization to the device i.e. the capabilities supported on a device
   * @param { CodeableConcept } systemType  - The code for standard used to communicate
   * @param { string } version - The version of the standard used to communicate
   * @returns { DeviceBuilder } This DeviceBuilder
   */
  public addSpecialization(systemType: CodeableConcept, version: string): DeviceBuilder {
    if (!Array.isArray(this.device.specialization)) {
      this.device.specialization = []
    }
    else {
      this.device.specialization.forEach(specialization => {
        specialization.systemType.coding.forEach(coding => {
          systemType.coding.forEach(systemTypeCoding => {
            if (
              coding.system === systemTypeCoding.system &&
              coding.code === systemTypeCoding.code
            ) {
              throw new Error(
                'A specialization with a systemType with the same system and coding already exists on the device.'
              )
            }
          })
        })
      })
    }
    const specialization: DeviceSpecialization = {
      systemType,
      version
    } as DeviceSpecialization
    this.device.specialization.push(specialization)
    return this
  }

  /**
   * Remove a specialization for the device based on a system and a code for the type of that specialization
   * @param { uri } system - the system specialization
   * @param { code } code - Symbol in syntax defined by the system
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeSpecialization(system: uri, code: code): boolean {
    this.device.specialization.forEach((specialization, index) => {
      specialization.systemType.coding.forEach(coding => {
        if (coding.system === system && coding.code === code) {
          this.device.specialization.splice(index, 1)
          return
        }
      })
    })
    return true
  }

  /**
   * Adds a version of the design of the device or software running on the device, to the device
   * @param { CodeableConcept } type - The type of the device version
   * @param { Identifier } component - A single component of the device version
   * @param { value } string - The version text
   * @returns { DeviceBuilder } This DeviceBuilder
   */
  public addVersion(
    type: CodeableConcept,
    component: Identifier,
    value: string
  ): DeviceBuilder {
    if (!Array.isArray(this.device.version)) {
      this.device.version = []
    }
    else {
      this.device.version.forEach(version => {
        version.type.coding.forEach(coding => {
          type.coding.forEach(versionCoding => {
            if (
              coding.system === versionCoding.system &&
              coding.code === versionCoding.code
            ) {
              throw new Error(
                'A version with a type with the same system and coding already exists on the device.'
              )
            }
          })
        })
      })
    }

    this.device.version.push({
      type,
      component,
      value,
    } as DeviceVersion)
    return this
  }

  /**
   * Removes the version from the device specified by a system and code for the type of version
   * @param { uri } system - the system property
   * @param { code } code - Symbol in syntax defined by the system
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeVersion(system: uri, code: code): boolean {
    this.device.version.forEach((version, index) => {
      version.type.coding.forEach(coding => {
        if (coding.system === system && coding.code === code) {
          this.device.version.splice(index, 1)
          return
        }
      })
    })
    return true
  }

  /**
   * Make a new object that implements a FHIR Device
   * @returns { Device } - an object that implements FHIR Device
   */
  private make(): Device {
    const device = {
      resourceType: 'Device',
      identifier: [
        {
          system: 'urn:ietf:rfc:3986',
          value: Guid.newGuid(),
        },
      ],
    } as Device
    return device
  }

  /**
   * Builds and returns the device
   * @returns { Device } - the device
   */
  public build(): Device {
    return this.device
  }
}
