import { ObservationBuilder } from './observation-builder'
import { ObservationComponentBuilder } from './observation-component-builder'
import { code, DeviceDefinition } from 'fhir'
import { IdentifierBuilder } from './identifier-builder'
import { DeviceManager } from './phg-core/device-manager'
import { CordovaDeviceManager } from './phg-core/cordova-device-manager'
import { PHGCoreDeviceManager } from './phg-core/phgcore-device-manager'
import { DeviceBuilder } from './device-builder'
import { DeviceConnector } from './phg-core/device-connector'
import { CordovaDeviceConnector } from './phg-core/cordova-device-connector'
import { PHGCoreDeviceConnector } from './phg-core/phgcore-device-connector'
import { MonicaDeviceConnector } from './phg-core/monica-device-connector'
import { TestDeviceConnector } from './phg-core/test-device-connector'
import { CodeableConceptBuilder } from './codeable-concept-builder'
import { DevicePropertyBuilder } from './device-property-builder'
import { Consumer } from './contour/consumer'
import { ConsumerBuilder } from './contour/consumer-builder'
import { PHGCoreConsumerBuilder } from './contour/phgcore-consumer-builder'


/** Class representing a DeviceMeasurementEngine */
export default class DeviceMeasurementEngine {
  /**
   * The DeviceManager allows for managing the interaction with external devices.
   * @private
   * @type { DeviceManager }
   * @ignore
   */
  private deviceManager: DeviceManager

  /**
   * Creates a new DeviceMeasurementEngine with a devicemanager for managing the interaction with external devices.
   * @param { DeviceManager } deviceManager - A manager used for managing the interaction with external devices.
   */
  constructor(deviceManager?: DeviceManager) {
    this.deviceManager = deviceManager
  }

  /**
   * Returns ObservationBuilder for manually building observations
   * @returns { ObservationBuilder } - The ObservationBuilder
   */
  public getObservationBuilder(): ObservationBuilder {
    return new ObservationBuilder()
  }

  /**
   * Returns ObservationComponentBuilder for manually building observations
   * @returns { ObservationComponentBuilder } - The ObservationComponentBuilder
   */
  public getObservationComponentBuilder(): ObservationComponentBuilder {
    return new ObservationComponentBuilder()
  }

  /**
   * Returns DeviceBuilder for manually building devices
   * @returns { DeviceBuilder } - The DeviceBuilder
   */
  public getDeviceBuilder(): DeviceBuilder {
    return new DeviceBuilder()
  }

  /**
   * Returns IdentifierBuilder for manually building identifiers
   * @returns { IdentifierBuilder } - The IdentifierBuilder
   */
  public getIdentifierBuilder(): IdentifierBuilder {
    return new IdentifierBuilder()
  }

  /**
   * Returns CodeableConceptBuilder for manually building CodeableConcepts
   * @returns { CodeableConceptBuilder } - The CodeableConceptBuilder
   */
  public getCodeableConceptBuilder(): CodeableConceptBuilder {
    return new CodeableConceptBuilder()
  }

  /**
   * Returns DevicePropertyBuilder for manually building DeviceProperties
   * @returns { DevicePropertyBuilder } - The DevicePropertyBuilder
   */
  public getDevicePropertyBuilder(): DevicePropertyBuilder {
    return new DevicePropertyBuilder()
  }

  /**
   * Returns all currently available devices of a specific type
   * @param { DeviceDefinition } deviceDefinition - A DeviceDefinition specifying the type of device
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of DeviceConnectors
   */
  public getAvailableDevices(
    deviceDefinition: DeviceDefinition
  ): Promise<DeviceConnector[]> {
    if (this.deviceManager == null) {
      throw new Error('DeviceMeasurementEngine has no DeviceManager')
    }
    if (deviceDefinition.type == null) {
      throw new Error('No type is specified for the deviceDefinition')
    }
    const codes: code[] = []
    deviceDefinition.type.coding.forEach(coding => {
      codes.push(coding.code)
    })
    return this.deviceManager.getAvailableDevices(codes)
  }

  /**
   * Searches for devices of a specific type
   * @param { DeviceDefinition } deviceDefinition - A DeviceDefinition specifying the type of device
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of DeviceConnectors
   */
  public searchForDevices(
    deviceDefinition: DeviceDefinition
  ): Promise<DeviceConnector[]> {
    if (this.deviceManager == null) {
      throw new Error('DeviceMeasurementEngine has no DeviceConnector')
    }
    if (deviceDefinition.type == null) {
      throw new Error('No type is specified for the deviceDefinition')
    }
    const codes: code[] = []
    deviceDefinition.type.coding.forEach(coding => {
      codes.push(coding.code)
    })
    return this.deviceManager.searchForDevices(codes)
  }
}

export {
  DeviceManager,
  DeviceConnector,
  CordovaDeviceManager,
  CordovaDeviceConnector,
  PHGCoreDeviceManager,
  PHGCoreDeviceConnector,
  TestDeviceConnector,
  MonicaDeviceConnector,
  Consumer,
  ConsumerBuilder,
  PHGCoreConsumerBuilder
}
