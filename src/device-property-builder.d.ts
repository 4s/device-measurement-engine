import { DeviceProperty, code, uri, CodeableConcept } from 'fhir';
/** A class representing a FHIR DevicePropertyBuilder */
export declare class DevicePropertyBuilder {
    /**
     * The DeviceProperty being built
     * @private
     * @type { DevicePropertyBuilder }
     * @ignore
     */
    private deviceProperty;
    /**
     * Creates a new DevicePropertyBuilder
     */
    constructor();
    /**
     * Sets the type of property. Multiple code systems are allowed to identify the type of device, e.g. SNOMED-CT or MDC
     * @param { CodeableConcept } type - the type of property
     * @returns { DevicePropertyBuilder } - This DevicePropertyBuilder
     */
    setType(type: CodeableConcept): DevicePropertyBuilder;
    /**
     * Removes the type of property.
     * @returns { boolean } A boolean indicating whether the operation was successful
     */
    removeType(): boolean;
    /**
     * Adds a property value as a code
     * @param { CodeableConcept } valueCode - property value as a code
     */
    addValueCode(valueCode: CodeableConcept): DevicePropertyBuilder;
    /**
     * Removes a property value as a code from the DeviceProperty based on a system and a code
     * @param { uri } system - Identity of the terminology system
     * @param { code } code - Symbol in syntax defined by the system
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueCode(system: uri, code: code): boolean;
    /**
     * Creates a new DeviceProperty
     * @returns { DeviceProperty } The deviceProperty
     */
    private make;
    /**
     * Builds and returns the DeviceProperty
     * @returns { DeviceProperty } - the DeviceProperty
     */
    build(): DeviceProperty;
}
