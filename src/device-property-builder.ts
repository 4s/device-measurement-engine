import {
  DeviceProperty,
  code,
  uri,
  CodeableConcept,
} from 'fhir'

/** A class representing a FHIR DevicePropertyBuilder */
export class DevicePropertyBuilder {
  /**
   * The DeviceProperty being built
   * @private
   * @type { DevicePropertyBuilder }
   * @ignore
   */
  private deviceProperty: DeviceProperty

  /**
   * Creates a new DevicePropertyBuilder
   */
  public constructor() {
    this.deviceProperty = this.make()
  }

  /**
   * Sets the type of property. Multiple code systems are allowed to identify the type of device, e.g. SNOMED-CT or MDC
   * @param { CodeableConcept } type - the type of property
   * @returns { DevicePropertyBuilder } - This DevicePropertyBuilder
   */
  public setType(type: CodeableConcept): DevicePropertyBuilder {
    this.deviceProperty.type = type
    return this
  }

  /**
   * Removes the type of property.
   * @returns { boolean } A boolean indicating whether the operation was successful
   */
  public removeType(): boolean {
    delete this.deviceProperty.type
    return true
  }

  /**
   * Adds a property value as a code
   * @param { CodeableConcept } valueCode - property value as a code
   */
  public addValueCode(valueCode: CodeableConcept): DevicePropertyBuilder {
    if (!Array.isArray(this.deviceProperty.valueCode)) {
      this.deviceProperty.valueCode = []
    }
    this.deviceProperty.valueCode.push(valueCode)
    return this
  }

  /**
   * Removes a property value as a code from the DeviceProperty based on a system and a code
   * @param { uri } system - Identity of the terminology system
   * @param { code } code - Symbol in syntax defined by the system
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueCode(system: uri, code: code): boolean {
    this.deviceProperty.valueCode.forEach((codeableConcept, index) => {
      codeableConcept.coding.forEach(coding => {
        if (coding.system === system && coding.code === code) {
          this.deviceProperty.valueCode.splice(index, 1)
          return
        }
      })
    })
    return true
  }

  /**
   * Creates a new DeviceProperty
   * @returns { DeviceProperty } The deviceProperty
   */
  private make(): DeviceProperty {
    const deviceProperty: DeviceProperty = {} as DeviceProperty
    return deviceProperty
  }

  /**
   * Builds and returns the DeviceProperty
   * @returns { DeviceProperty } - the DeviceProperty
   */
  public build(): DeviceProperty {
    return this.deviceProperty
  }
}
