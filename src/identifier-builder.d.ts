import { Identifier, code, uri, dateTime } from 'fhir';
/** Class representing a FHIR Identifier builder */
export declare class IdentifierBuilder {
    /**
     * The identifier being built
     * @private
     * @type { Identifier }
     * @ignore
     */
    private identifier;
    /**
     * Create a new FHIR ObservationBuilder
     */
    constructor();
    /**
     * Sets the use of the identifier
     * usual | official | temp | secondary (If known)
     * @param { code } use -  the use to set
     * @returns { IdentifierBuilder } this IdentifierBuilder
     */
    setUse(use: code): IdentifierBuilder;
    /**
     * Removes the use from the identifier
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeUse(): boolean;
    /**
     * Sets the type of the identifier as a CodeableConcept
     * @param { string } text Plain text representation of the concept
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Version of the system - if relevant
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { IdentifierBuilder } this IdentifierBuilder
     */
    setType(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): IdentifierBuilder;
    /**
     * Removes the type from the identifier
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeType(): boolean;
    /**
     * Sets the namespace for the identifier value
     * @param { uri } system - the namespace to set
     * @returns { IdentifierBuilder } this IdentifierBuilder
     */
    setSystem(system: uri): IdentifierBuilder;
    /**
     * Removes the system from the identifier
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeSystem(): boolean;
    /**
     * Sets the (unique) value of the identifier
     * @param { string } value -  the value to set
     * @returns { IdentifierBuilder } this IdentifierBuilder
     */
    setValue(value: string): IdentifierBuilder;
    /**
     * Removes the value from the identifier
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeValue(): boolean;
    /**
     * Sets the period wherin the identifier is in effect
     * @param { dateTime } start - The start of the period
     * @param { dateTime } end - The end of the period
     * @returns { IdentifierBuilder } this IdentifierBuilder
     */
    setPeriod(start?: dateTime, end?: dateTime): IdentifierBuilder;
    /**
     * Removes the period from the identifier
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removePeriod(): boolean;
    /**
     * Sets the assigner of the identifier
     * @param { string } reference - Literal reference, Relative, internal or absolute URL
     * @param { Identifier } identifier - Logical reference, when literal reference is not known
     * @param { string } display - Text alternative for the resource
     * @returns { IdentifierBuilder } this IdentifierBuilder
     */
    setAssigner(reference?: string, identifier?: Identifier, display?: string): IdentifierBuilder;
    /**
     * Removes the assigner from the identifier
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeAssigner(): boolean;
    /**
     * Returns the identifier
     * @returns { Identifier } the identifier
     */
    build(): Identifier;
}
