import { Observation, Quantity, QuantityComparator, ObservationDefinition, CodeableConcept, decimal, code, uri, dateTime, integer, Period, Range, Ratio, positiveInt, SampledData, time, Identifier, Reference, instant, ObservationComponent } from 'fhir';
/** Class representing a FHIR Observation builder */
export declare class ObservationBuilder {
    /**
     * The observation being built
     * @private
     * @type { Observation }
     * @ignore
     */
    protected observation: Observation;
    /**
     * Creates a new FHIR ObservationBuilder
     */
    constructor();
    /**
     * Returns all identifiers for observation
     * @returns { Identifier[] } - the identifiers
     */
    getIdentifiers(): Identifier[];
    /**
     * Returns identifier from specific system if one exists
     * @param { uri } system - the system from which to return the identifier
     * @returns { Identifier } the identifier
     */
    getIdentifier(system: uri): Identifier;
    /**
     * Replaces the identifier array with a new array containing a single identifier
     * @param { Identifier } identifier - the identifier to be set
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setIdentifier(identifier: Identifier): ObservationBuilder;
    /**
     * Appends an identifier to the observation, if an identifier within the system does not already exist
     * @param { Identifier } identifier - the identifier to be appended
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    addIdentifier(identifier: Identifier): ObservationBuilder;
    /**
     * Removes an identifier specified by the system-property, if one exists
     * @param { uri } system - the system property
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeIdentifier(system: uri): boolean;
    /**
     * Sets the reference to the resource from which this observation was requested.
     * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
     * @param { string } reference - literal reference, Relative, internal or absolute URL
     * @param { Identifier } identifier - logical reference, when literal reference is not known
     * @param { string } display - text alternative for the resource
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setBasedOn(reference?: string, identifier?: Identifier, display?: string): ObservationBuilder;
    /**
     * Returns the basedOn of the observation
     * @returns { Reference[] } the basedOn
     */
    getBasedOn(): Reference[];
    /**
     * Removes the basedOn field from the observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeBasedOn(): boolean;
    /**
     * Returns the status of the observation
     * @returns { code } the status
     */
    getStatus(): code;
    /**
     * Sets the status code of the observation. The code must be part of the following set:
     * registered | preliminary | final | amended | corrected | cancelled | entered-in-error | unknown
     * @param { codee } code - the new status code
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setStatus(code: code): ObservationBuilder;
    /**
     * Sets the code for the type of observation
     * @param { string } text - Plain text representation of the type of observation
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setCode(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): ObservationBuilder;
    /**
     * Removes the code for the type of observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeCode(): boolean;
    /**
     * Sets the subject of the observation
     * @param { string } reference - literal reference, Relative, internal or absolute URL
     * @param { Identifier } identifier - logical reference, when literal reference is not known
     * @param { string } display - text alternative for the resource
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setSubject(reference?: string, identifier?: Identifier, display?: string): ObservationBuilder;
    /**
     * Removes the subject from the observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeSubject(): boolean;
    /**
     * Sets the clinically relevant time for observation
     * @param dateTime the datetime to be set
     * @returns this ObservationBuilder
     */
    setEffectiveDateTime(dateTime: dateTime): ObservationBuilder;
    /**
     * Get the clinically relevant time for observation
     * @returns the effectiveDateTime
     */
    getEffectiveDateTime(): dateTime;
    /**
     * Removes the clinicially relevant time for observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeEffectiveDateTime(): boolean;
    /**
     * Sets the clinically relevant time-period for observation.
     * If either start or end is left out, the period is considered open-ended.
     * Both should not be left out simoultaneously.
     * @param { dateTime } start - the start dateTime of the period
     * @param { dateTime } end - the end dateTime of the period
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setEffectivePeriod(start?: dateTime, end?: dateTime): ObservationBuilder;
    /**
     * Removes the clinicially relevant time-period for observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeEffectivePeriod(): boolean;
    /**
     * Sets the clinically relevant instant for observation
     * @param { instant } start - the instant
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setEffectiveInstant(instant: instant): ObservationBuilder;
    /**
     * Removes the clinicially relevant instant for observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeEffectiveInstant(): boolean;
    /**
     * Set a reference to the performer of the observation
     * @param { string } reference - literal reference, Relative, internal or absolute URL
     * @param { Identifier } identifier - logical reference, when literal reference is not known
     * @param { string } display - text alternative for the resource
     */
    setPerformer(reference?: string, identifier?: Identifier, display?: string): ObservationBuilder;
    /**
     * Remove the reference to the performer of the observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removePerformer(): ObservationBuilder;
    /**
     * Returns the value from the observation
     * @returns { boolean | dateTime | integer | string | time | Period | Quantity | CodeableConcept | Range | Ratio | SampledData } - The value
     */
    getValue(): boolean | dateTime | integer | string | time | Period | Quantity | CodeableConcept | Range | Ratio | SampledData;
    /**
     * Set the value of the observation as a boolean, if no value has been set on the observation.
     * @param { boolean } value - the value to be set
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueBoolean(value: boolean): ObservationBuilder;
    /**
     * Removes valueBoolean from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueBoolean(): boolean;
    /**
     * Set the value of the observation as a dateTime, if no value has been set on the observation.
     * @param { dateTime } value - the value to be set
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueDateTime(value: dateTime): ObservationBuilder;
    /**
     * Removes valueDateTime from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueDateTime(): boolean;
    /**
     * Set the value of the observation as an integer, if no value has been set on the observation.
     * @param { integer } value - the value to be set
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueInteger(value: integer): ObservationBuilder;
    /**
     * Removes valueInteger from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueInteger(): boolean;
    /**
     * Set the value of the observation as a string, if no value has been set on the observation.
     * @param { string } value - the value to be set
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueString(value: string): ObservationBuilder;
    /**
     * Removes valueString from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueString(): boolean;
    /**
     * Set the value of the observation as a time, if no value has been set on the observation.
     * @param { time } value - the value to be set
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueTime(value: time): ObservationBuilder;
    /**
     * Removes valueTime from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueTime(): boolean;
    /**
     * Set the value of the observation as a Period, if no value has been set on the observation.
     * If either start or end is left out, the period is considered open-ended.
     * Both should not be left out simoultaneously.
     * @param { dateTime } start - the start dateTime of the period
     * @param { dateTime } end - the end dateTime of the period
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValuePeriod(start?: dateTime, end?: dateTime): ObservationBuilder;
    /**
     * Removes valuePeriod from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValuePeriod(): boolean;
    /**
     * Set the value of the observation as a Quantity with a value and a unit in a specific coding system, if no value
     * has been set on the observation.
     * @param { decimal } value - Numerical value (with implicit precision)
     * @param { code } comparator - < | <= | >= | > - how to understand the value
     * @param { string } unit - Unit representation
     * @param { uri } system - System that defines coded unit form
     * @param { code } code - Coded form of the unit
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueQuantity(value?: decimal, comparator?: QuantityComparator, unit?: string, system?: uri, code?: code): ObservationBuilder;
    /**
     * Removes valueQuantity from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueQuantity(): boolean;
    /**
     * Set the value of the observation as a CodeableConcept, if no value has been set on the observation.
     * @param { string } text - Plain text representation of the concept
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueCodeableConcept(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): ObservationBuilder;
    /**
     * Removes valueCodeableConcept from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueCodeableConcept(): boolean;
    /**
     * Set the value of the observation as a Range between two values, if no value has been set on the observation.
     * Whether to provide a specific coding system is optional.
     * @param { decimal } lowValue - The lower value of the range
     * @param { decimal } highValue - The high value of the range
     * @param { string } unit - Unit representation
     * @param { uri } system - System that defines coded unit form
     * @param { code } code - Coded form of the unit
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueRange(lowValue?: decimal, highValue?: decimal, unit?: string, system?: uri, code?: code): ObservationBuilder;
    /**
     * Removes valueRange from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueRange(): boolean;
    /**
     * Set the value of the observation as a Ratio, if no value has been set on the observation.
     * Whether to provide a specific coding system is optional.
     * @param { decimal } numerator - The numerator in the ratio
     * @param { decimal } denominator - The denominator in the ratio
     * @param { string } unit - Unit representation
     * @param { uri } system - System that defines coded unit form
     * @param { code } code - Coded form of the unit
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueRatio(numerator?: decimal, denominator?: decimal, unit?: string, system?: uri, code?: code): ObservationBuilder;
    /**
     * Removes valueRatio from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueRatio(): boolean;
    /**
     * Set the value of the observation as a collection of SampledData, if no value has been set on the observation.
     * @param { decimal } originValue - The zero value
     * @param { string } originUnit - Unit representation of the zero value
     * @param { uri } originSystem - System that defines coded unit form of the zero value
     * @param { code } originCode - Coded form of the unit of the zero value
     * @param { decimal } period - Number of milliseconds between samples
     * @param { positiveInt } dimensions - Number of sample points at each time point
     * @param { string } data - Decimal values with spaces, or "E" | "U" | "L"
     * @param { decimal } factor - Multiply data by this before adding to origin
     * @param { decimal } lowerLimit - Lower limit of detection
     * @param { decimal } upperLimit - Upper limit of detection
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setValueSampledData(originValue: decimal, originUnit: string, originSystem: uri, originCode: code, period: decimal, dimensions: positiveInt, data: string, factor?: decimal, lowerLimit?: decimal, upperLimit?: decimal): ObservationBuilder;
    /**
     * Removes valueSampledData from observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueSampledData(): boolean;
    /**
     * Sets the reason for the data being absent
     * @param { string } text - Plain text representation of the reason for the data being absent
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setDataAbsentReason(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): ObservationBuilder;
    /**
     * Removes the reason for the data being absent
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeDataAbsentReason(): boolean;
    /**
     * Adds an interpretation for the observation
     * @param { string } text - Plain text representation of the interpretation for the observation
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    addInterpretation(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): ObservationBuilder;
    /**
     * Clear interpretations for the observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    clearInterpretations(): boolean;
    /**
     * Sets the method with which the observation was performed
     * @param { string } text - Plain text representation of the method with which the observation was performed
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setMethod(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): ObservationBuilder;
    /**
     * Removes the method with which the observation was performed
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeMethod(): boolean;
    /**
     * Sets the reference to the device with which the observation was performed.
     * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
     * @param { string } reference - literal reference, Relative, internal or absolute URL
     * @param { Identifier } identifier - logical reference, when literal reference is not known
     * @param { string } display - text alternative for the resource
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setDevice(reference?: string, identifier?: Identifier, display?: string): ObservationBuilder;
    /**
     * Removes the reference to the device with which the observation was performed
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeDevice(): boolean;
    /**
     * Returns the device of the observation
     */
    getDevice(): Reference;
    /**
     * Sets the related resource that belongs to the Observation group.
     * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
     * @param { string } reference - literal reference, Relative, internal or absolute URL
     * @param { Identifier } identifier - logical reference, when literal reference is not known
     * @param { string } display - text alternative for the resource
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setHasMember(reference?: string, identifier?: Identifier, display?: string): ObservationBuilder;
    /**
     * Returns the device of the observation
     */
    getHasMember(): Reference[];
    /**
     * Adds a related resource that belongs to the Observation group.
     * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
     * @param { string } reference - literal reference, relative, internal or absolute URL
     * @param { Identifier } identifier - logical reference, when literal reference is not known
     * @param { string } display - text alternative for the resource
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    addHasMember(reference?: string, identifier?: Identifier, display?: string): ObservationBuilder;
    /**
     * Removes all of the related resources that was added as a hasMember.
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeAllHasMembers(): boolean;
    /**
     * Removes one of the related resource that was added as a hasMember, identified by a literal or logical reference.
     * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
     * @param { string } reference - literal reference, Relative, internal or absolute URL
     * @param { Identifier } identifier - logical reference, when literal reference is not known
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeHasMember(reference?: string, identifier?: Identifier): boolean;
    /**
     * Returns all components for observation
     * @returns { ObservationComponent[] } - the components
     */
    getComponents(): ObservationComponent[];
    /**
     * Returns a component with a specific system and code
     * @param { uri } system - The system
     * @param { code } code - The code
     * @returns { ObservationComponent } The component with the system and code
     */
    getComponent(system: uri, theCode: string): ObservationComponent;
    /**
     * Replaces the component array with the provided array
     * @param { ObservationComponent[] } components - the component array to be set
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    setComponents(components: ObservationComponent[]): ObservationBuilder;
    /**
     * Appends a component to the observation
     * @param { ObservationComponent } component - the component to be appended
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    addComponent(component: ObservationComponent): ObservationBuilder;
    /**
     * Removes all components from the observation
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeComponents(): boolean;
    /**
     * Sets an existing observation for this ObservationBuilder to allow for manipulating, or interacting with, the
     * observation through this ObservationBuilder
     * @param { Observation } observation - The observation to set
     * @returns { ObservationBuilder } This ObservationBuilder
     */
    setObservation(observation: Observation): ObservationBuilder;
    /**
     * Create a FHIR Observation resource from a FHIR ObservationDefinition resource, and sets it in the observationbuilder
     * @param { IObservationDefinition } observationDefinition - the FHIR ObservationDefinition resource to create the observation from
     * @returns { ObservationBuilder } - this ObservationBuilder
     */
    observationFromDefinition(observationDefinition: ObservationDefinition): ObservationBuilder;
    /**
     * Checks if value[x] is set for the observation
     * @returns { boolean } - a boolean value indication whether the value is set
     */
    private checkIfValueIsSet;
    /**
     * Checks if effective[x] is set for the observation
     * @returns { boolean } - a boolean value indication whether the effective[x] is set
     */
    private checkIfEffectiveSet;
    /**
     * Make a new object that implements a FHIR Observation
     * @returns { Observation } - an object that implements FHIR Observation
     */
    private make;
    /**
     * Builds and returns the observation
     * @returns { Observation } - the observation
     */
    build(): Observation;
}
