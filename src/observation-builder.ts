
import {
  Observation,
  Quantity,
  QuantityComparator,
  ObservationDefinition,
  CodeableConcept,
  ObservationReferenceRange,
  decimal,
  code,
  uri,
  Coding,
  dateTime,
  integer,
  Period,
  Range,
  Ratio,
  positiveInt,
  SampledData,
  time,
  Identifier,
  Reference,
  instant,
  ObservationComponent,
} from 'fhir'
import { Guid } from './helpers/guid'

/** Class representing a FHIR Observation builder */
export class ObservationBuilder {
  /**
   * The observation being built
   * @private
   * @type { Observation }
   * @ignore
   */
  protected observation: Observation

  /**
   * Creates a new FHIR ObservationBuilder
   */
  public constructor() {
    this.observation = this.make()
  }

  /**
   * Returns all identifiers for observation
   * @returns { Identifier[] } - the identifiers
   */
  public getIdentifiers(): Identifier[] {
    if (Array.isArray(this.observation.identifier)) {
      return this.observation.identifier
    }
    return []
  }

  /**
   * Returns identifier from specific system if one exists
   * @param { uri } system - the system from which to return the identifier
   * @returns { Identifier } the identifier
   */
  public getIdentifier(system: uri): Identifier {
    let returnIdentifier: Identifier
    this.observation.identifier.forEach(identifier => {
      if (identifier.system === system) {
        returnIdentifier = identifier
        return
      }
    })
    return returnIdentifier
  }

  /**
   * Replaces the identifier array with a new array containing a single identifier
   * @param { Identifier } identifier - the identifier to be set
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setIdentifier(identifier: Identifier): ObservationBuilder {
    this.observation.identifier = [identifier]
    return this
  }

  /**
   * Appends an identifier to the observation, if an identifier within the system does not already exist
   * @param { Identifier } identifier - the identifier to be appended
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public addIdentifier(identifier: Identifier): ObservationBuilder {
    if (Array.isArray(this.observation.identifier)) {
      if (typeof this.getIdentifier(identifier.system) === 'undefined') {
        this.observation.identifier.push(identifier)
      } else {
        throw new Error(
          'An identifier in that system already exists on the observation.'
        )
      }
    } else {
      this.observation.identifier = [identifier]
    }
    return this
  }

  /**
   * Removes an identifier specified by the system-property, if one exists
   * @param { uri } system - the system property
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeIdentifier(system: uri): boolean {
    let returnValue = false
    this.observation.identifier.forEach((identifier, index) => {
      if (identifier.system === system) {
        this.observation.identifier.splice(index, 1)
        returnValue = true
      }
    })
    return returnValue
  }

  /**
   * Sets the reference to the resource from which this observation was requested.
   * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
   * @param { string } reference - literal reference, Relative, internal or absolute URL
   * @param { Identifier } identifier - logical reference, when literal reference is not known
   * @param { string } display - text alternative for the resource
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setBasedOn(
    reference?: string,
    identifier?: Identifier,
    display?: string
  ): ObservationBuilder {
    const basedOn: Reference = {} as Reference
    basedOn.reference = reference
    if (typeof basedOn.reference === 'undefined') {
      basedOn.identifier = identifier
    }
    basedOn.display = display
    this.observation.basedOn = [basedOn]
    return this
  }

  /**
   * Returns the basedOn of the observation
   * @returns { Reference[] } the basedOn
   */
  public getBasedOn(): Reference[] {
    return this.observation.basedOn
  }

  /**
   * Removes the basedOn field from the observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeBasedOn(): boolean {
    delete this.observation.basedOn
    return true
  }


  /**
   * Returns the status of the observation
   * @returns { code } the status
   */
  public getStatus(): code {
    return this.observation.status
  }


  /**
   * Sets the status code of the observation. The code must be part of the following set:
   * registered | preliminary | final | amended | corrected | cancelled | entered-in-error | unknown
   * @param { codee } code - the new status code
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setStatus(code: code): ObservationBuilder {
    if (
      code !== 'registered' &&
      code !== 'preliminary' &&
      code !== 'final' &&
      code !== 'amended' &&
      code !== 'corrected' &&
      code !== 'cancelled' &&
      code !== 'entered-in-error' &&
      code !== 'unknown'
    ) {
      throw new Error(
        'The provided code is not part of the valid set of Observation status codes'
      )
    }
    this.observation.status = code
    return this
  }
  /**
   * Sets the code for the type of observation
   * @param { string } text - Plain text representation of the type of observation
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setCode(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): ObservationBuilder {
    const codeableConcept: CodeableConcept = {
      coding: [
        {
          system,
          version,
          code,
          display,
          userSelected,
        },
      ],
      text,
    } as CodeableConcept
    this.observation.code = codeableConcept
    return this
  }

  /**
   * Removes the code for the type of observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeCode(): boolean {
    delete this.observation.code
    return true
  }

  /**
   * Sets the subject of the observation
   * @param { string } reference - literal reference, Relative, internal or absolute URL
   * @param { Identifier } identifier - logical reference, when literal reference is not known
   * @param { string } display - text alternative for the resource
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setSubject(
    reference?: string,
    identifier?: Identifier,
    display?: string
  ): ObservationBuilder {
    const subject: Reference = {
      reference,
      identifier,
      display,
    } as Reference
    this.observation.subject = subject
    return this
  }

  /**
   * Removes the subject from the observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeSubject(): boolean {
    delete this.observation.subject
    return true
  }

  /**
   * Sets the clinically relevant time for observation
   * @param dateTime the datetime to be set
   * @returns this ObservationBuilder
   */
  public setEffectiveDateTime(dateTime: dateTime): ObservationBuilder {
    if (this.checkIfEffectiveSet()) {
      throw new Error(
        'Cannot set effectiveDateTime. effective[x] is already set for observation.'
      )
    }
    this.observation.effectiveDateTime = dateTime
    return this
  }


  /**
   * Get the clinically relevant time for observation
   * @returns the effectiveDateTime
   */
  public getEffectiveDateTime(): dateTime {
    return this.observation.effectiveDateTime
  }

  /**
   * Removes the clinicially relevant time for observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeEffectiveDateTime(): boolean {
    delete this.observation.effectiveDateTime
    return true
  }

  /**
   * Sets the clinically relevant time-period for observation.
   * If either start or end is left out, the period is considered open-ended.
   * Both should not be left out simoultaneously.
   * @param { dateTime } start - the start dateTime of the period
   * @param { dateTime } end - the end dateTime of the period
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setEffectivePeriod(
    start?: dateTime,
    end?: dateTime
  ): ObservationBuilder {
    const period: Period = {
      start,
      end,
    } as Period
    if (this.checkIfEffectiveSet()) {
      throw new Error(
        'Cannot set effectiveDateTime. effective[x] is already set for observation.'
      )
    }
    this.observation.effectivePeriod = period
    return this
  }

  /**
   * Removes the clinicially relevant time-period for observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeEffectivePeriod(): boolean {
    delete this.observation.effectivePeriod
    return true
  }

  /**
   * Sets the clinically relevant instant for observation
   * @param { instant } start - the instant
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setEffectiveInstant(instant: instant): ObservationBuilder {
    if (this.checkIfEffectiveSet()) {
      throw new Error(
        'Cannot set effectiveInstant. effective[x] is already set for observation.'
      )
    }
    this.observation.effectiveInstant = instant
    return this
  }

  /**
   * Removes the clinicially relevant instant for observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeEffectiveInstant(): boolean {
    delete this.observation.effectiveInstant
    return true
  }

  /**
   * Set a reference to the performer of the observation
   * @param { string } reference - literal reference, Relative, internal or absolute URL
   * @param { Identifier } identifier - logical reference, when literal reference is not known
   * @param { string } display - text alternative for the resource
   */
  public setPerformer(
    reference?: string,
    identifier?: Identifier,
    display?: string
  ): ObservationBuilder {
    const performer: Reference = {} as Reference
    performer.reference = reference
    if (typeof performer.reference === 'undefined') {
      performer.identifier = identifier
    }
    performer.display = display
    this.observation.performer = [performer]
    return this
  }

  /**
   * Remove the reference to the performer of the observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removePerformer(): ObservationBuilder {
    delete this.observation.performer
    return this
  }

  /**
   * Returns the value from the observation
   * @returns { boolean | dateTime | integer | string | time | Period | Quantity | CodeableConcept | Range | Ratio | SampledData } - The value
   */
  public getValue():
    | boolean
    | dateTime
    | integer
    | string
    | time
    | Period
    | Quantity
    | CodeableConcept
    | Range
    | Ratio
    | SampledData {
    if (this.observation.valueBoolean != null) {
      return this.observation.valueBoolean
    } else if (this.observation.valueCodeableConcept != null) {
      return this.observation.valueCodeableConcept
    } else if (this.observation.valueDateTime != null) {
      return this.observation.valueDateTime
    } else if (this.observation.valueInteger != null) {
      return this.observation.valueInteger
    } else if (this.observation.valuePeriod != null) {
      return this.observation.valuePeriod
    } else if (this.observation.valueQuantity != null) {
      return this.observation.valueQuantity
    } else if (this.observation.valueRange != null) {
      return this.observation.valueRange
    } else if (this.observation.valueRatio != null) {
      return this.observation.valueRatio
    } else if (this.observation.valueSampledData != null) {
      return this.observation.valueSampledData
    } else if (this.observation.valueString != null) {
      return this.observation.valueString
    } else if (this.observation.valueTime != null) {
      return this.observation.valueTime
    } else {
      return null
    }
  }

  /**
   * Set the value of the observation as a boolean, if no value has been set on the observation.
   * @param { boolean } value - the value to be set
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueBoolean(value: boolean): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueBoolean. Value is already set for observation.'
      )
    }
    this.observation.valueBoolean = value

    return this
  }

  /**
   * Removes valueBoolean from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueBoolean(): boolean {
    delete this.observation.valueBoolean
    return true
  }

  /**
   * Set the value of the observation as a dateTime, if no value has been set on the observation.
   * @param { dateTime } value - the value to be set
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueDateTime(value: dateTime): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueDateTime. Value is already set for observation.'
      )
    }
    this.observation.valueDateTime = value

    return this
  }

  /**
   * Removes valueDateTime from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueDateTime(): boolean {
    delete this.observation.valueDateTime
    return true
  }

  /**
   * Set the value of the observation as an integer, if no value has been set on the observation.
   * @param { integer } value - the value to be set
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueInteger(value: integer): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueInteger. Value is already set for observation.'
      )
    }
    this.observation.valueInteger = value

    return this
  }

  /**
   * Removes valueInteger from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueInteger(): boolean {
    delete this.observation.valueInteger
    return true
  }

  /**
   * Set the value of the observation as a string, if no value has been set on the observation.
   * @param { string } value - the value to be set
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueString(value: string): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueString. Value is already set for observation.'
      )
    }
    this.observation.valueString = value

    return this
  }

  /**
   * Removes valueString from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueString(): boolean {
    delete this.observation.valueString
    return true
  }

  /**
   * Set the value of the observation as a time, if no value has been set on the observation.
   * @param { time } value - the value to be set
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueTime(value: time): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueTime. Value is already set for observation.'
      )
    }
    this.observation.valueTime = value

    return this
  }

  /**
   * Removes valueTime from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueTime(): boolean {
    delete this.observation.valueTime
    return true
  }

  /**
   * Set the value of the observation as a Period, if no value has been set on the observation.
   * If either start or end is left out, the period is considered open-ended.
   * Both should not be left out simoultaneously.
   * @param { dateTime } start - the start dateTime of the period
   * @param { dateTime } end - the end dateTime of the period
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValuePeriod(start?: dateTime, end?: dateTime): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valuePeriod. Value is already set for observation.'
      )
    }
    this.observation.valuePeriod = {
      start,
      end,
    } as Period

    return this
  }

  /**
   * Removes valuePeriod from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValuePeriod(): boolean {
    delete this.observation.valuePeriod
    return true
  }

  /**
   * Set the value of the observation as a Quantity with a value and a unit in a specific coding system, if no value
   * has been set on the observation.
   * @param { decimal } value - Numerical value (with implicit precision)
   * @param { code } comparator - < | <= | >= | > - how to understand the value
   * @param { string } unit - Unit representation
   * @param { uri } system - System that defines coded unit form
   * @param { code } code - Coded form of the unit
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueQuantity(
    value?: decimal,
    comparator?: QuantityComparator,
    unit?: string,
    system?: uri,
    code?: code
  ): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueQuantity. Value is already set for observation.'
      )
    }
    const valueQuantity: Quantity = {} as Quantity

    valueQuantity.value = value
    valueQuantity.comparator = comparator
    valueQuantity.unit = unit
    valueQuantity.system = system
    valueQuantity.code = code

    this.observation.valueQuantity = valueQuantity
    return this
  }

  /**
   * Removes valueQuantity from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueQuantity(): boolean {
    delete this.observation.valueQuantity
    return true
  }

  /**
   * Set the value of the observation as a CodeableConcept, if no value has been set on the observation.
   * @param { string } text - Plain text representation of the concept
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueCodeableConcept(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueCodeableConcept. Value is already set for observation.'
      )
    }
    const codeableConcept: CodeableConcept = {} as CodeableConcept
    const coding: Coding = {} as Coding

    coding.system = system
    coding.version = version
    coding.code = code
    coding.display = display
    coding.userSelected = userSelected

    codeableConcept.coding = [coding]
    codeableConcept.text = text

    this.observation.valueCodeableConcept = codeableConcept
    return this
  }

  /**
   * Removes valueCodeableConcept from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueCodeableConcept(): boolean {
    delete this.observation.valueCodeableConcept
    return true
  }

  /**
   * Set the value of the observation as a Range between two values, if no value has been set on the observation.
   * Whether to provide a specific coding system is optional.
   * @param { decimal } lowValue - The lower value of the range
   * @param { decimal } highValue - The high value of the range
   * @param { string } unit - Unit representation
   * @param { uri } system - System that defines coded unit form
   * @param { code } code - Coded form of the unit
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueRange(
    lowValue?: decimal,
    highValue?: decimal,
    unit?: string,
    system?: uri,
    code?: code
  ): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueRange. Value is already set for observation.'
      )
    }
    const lowQuantity: Quantity = {
      value: lowValue,
      unit,
      system,
      code,
    } as Quantity

    const highQuantity: Quantity = {
      value: highValue,
      unit,
      system,
      code,
    } as Quantity

    const range: Range = {
      low: lowQuantity,
      high: highQuantity,
    } as Range

    this.observation.valueRange = range

    return this
  }

  /**
   * Removes valueRange from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueRange(): boolean {
    delete this.observation.valueRange
    return true
  }

  /**
   * Set the value of the observation as a Ratio, if no value has been set on the observation.
   * Whether to provide a specific coding system is optional.
   * @param { decimal } numerator - The numerator in the ratio
   * @param { decimal } denominator - The denominator in the ratio
   * @param { string } unit - Unit representation
   * @param { uri } system - System that defines coded unit form
   * @param { code } code - Coded form of the unit
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueRatio(
    numerator?: decimal,
    denominator?: decimal,
    unit?: string,
    system?: uri,
    code?: code
  ): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueRatio. Value is already set for observation.'
      )
    }
    const numeratorQuantity: Quantity = {
      value: numerator,
      unit,
      system,
      code,
    } as Quantity

    const denominatorQuantity: Quantity = {
      value: denominator,
      unit,
      system,
      code,
    } as Quantity

    const ratio: Ratio = {
      numerator: numeratorQuantity,
      denominator: denominatorQuantity,
    } as Ratio

    this.observation.valueRatio = ratio

    return this
  }

  /**
   * Removes valueRatio from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueRatio(): boolean {
    delete this.observation.valueRatio
    return true
  }

  /**
   * Set the value of the observation as a collection of SampledData, if no value has been set on the observation.
   * @param { decimal } originValue - The zero value
   * @param { string } originUnit - Unit representation of the zero value
   * @param { uri } originSystem - System that defines coded unit form of the zero value
   * @param { code } originCode - Coded form of the unit of the zero value
   * @param { decimal } period - Number of milliseconds between samples
   * @param { positiveInt } dimensions - Number of sample points at each time point
   * @param { string } data - Decimal values with spaces, or "E" | "U" | "L"
   * @param { decimal } factor - Multiply data by this before adding to origin
   * @param { decimal } lowerLimit - Lower limit of detection
   * @param { decimal } upperLimit - Upper limit of detection
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setValueSampledData(
    originValue: decimal,
    originUnit: string,
    originSystem: uri,
    originCode: code,
    period: decimal,
    dimensions: positiveInt,
    data: string,
    factor?: decimal,
    lowerLimit?: decimal,
    upperLimit?: decimal
  ): ObservationBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueSampledData. Value is already set for observation.'
      )
    }
    const originQuantity: Quantity = {
      value: originValue,
      unit: originUnit,
      system: originSystem,
      code: originCode,
    } as Quantity

    const sampledData = {
      origin: originQuantity,
      period,
      factor,
      lowerLimit,
      upperLimit,
      dimensions,
      data,
    } as SampledData

    this.observation.valueSampledData = sampledData

    return this
  }

  /**
   * Removes valueSampledData from observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueSampledData(): boolean {
    delete this.observation.valueSampledData
    return true
  }

  /**
   * Sets the reason for the data being absent
   * @param { string } text - Plain text representation of the reason for the data being absent
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setDataAbsentReason(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): ObservationBuilder {
    const codeableConcept: CodeableConcept = {
      coding: [
        {
          system,
          version,
          code,
          display,
          userSelected,
        },
      ],
      text,
    } as CodeableConcept
    this.observation.dataAbsentReason = codeableConcept
    return this
  }

  /**
   * Removes the reason for the data being absent
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeDataAbsentReason(): boolean {
    delete this.observation.dataAbsentReason
    return true
  }

  /**
   * Adds an interpretation for the observation
   * @param { string } text - Plain text representation of the interpretation for the observation
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public addInterpretation(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): ObservationBuilder {
    const codeableConcept: CodeableConcept = {
      coding: [
        {
          system,
          version,
          code,
          display,
          userSelected,
        },
      ],
      text,
    } as CodeableConcept
    this.observation.interpretation.push(codeableConcept)
    return this
  }

  /**
   * Clear interpretations for the observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public clearInterpretations(): boolean {
    this.observation.interpretation = []
    return true
  }

  /**
   * Sets the method with which the observation was performed
   * @param { string } text - Plain text representation of the method with which the observation was performed
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setMethod(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): ObservationBuilder {
    const codeableConcept: CodeableConcept = {
      coding: [
        {
          system,
          version,
          code,
          display,
          userSelected,
        },
      ],
      text,
    } as CodeableConcept
    this.observation.method = codeableConcept
    return this
  }

  /**
   * Removes the method with which the observation was performed
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeMethod(): boolean {
    delete this.observation.method
    return true
  }

  /**
   * Sets the reference to the device with which the observation was performed.
   * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
   * @param { string } reference - literal reference, Relative, internal or absolute URL
   * @param { Identifier } identifier - logical reference, when literal reference is not known
   * @param { string } display - text alternative for the resource
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setDevice(
    reference?: string,
    identifier?: Identifier,
    display?: string
  ): ObservationBuilder {
    const device: Reference = {} as Reference
    device.reference = reference
    if (typeof device.reference === 'undefined') {
      device.identifier = identifier
    }
    device.display = display
    this.observation.device = device
    return this
  }

  /**
   * Removes the reference to the device with which the observation was performed
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeDevice(): boolean {
    delete this.observation.device
    return true
  }

  /**
   * Returns the device of the observation
   */
  public getDevice(): Reference {
    return this.observation.device
  }

  /**
   * Sets the related resource that belongs to the Observation group.
   * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
   * @param { string } reference - literal reference, Relative, internal or absolute URL
   * @param { Identifier } identifier - logical reference, when literal reference is not known
   * @param { string } display - text alternative for the resource
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setHasMember(
    reference?: string,
    identifier?: Identifier,
    display?: string
  ): ObservationBuilder {
    const hasMember: Reference = {} as Reference
    hasMember.reference = reference
    if (typeof hasMember.reference === 'undefined') {
      hasMember.identifier = identifier
    }
    hasMember.display = display
    this.observation.hasMember = [hasMember]
    return this
  }

  /**
   * Returns the device of the observation
   */
  public getHasMember(): Reference[] {
    return this.observation.hasMember
  }


  /**
   * Adds a related resource that belongs to the Observation group.
   * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
   * @param { string } reference - literal reference, relative, internal or absolute URL
   * @param { Identifier } identifier - logical reference, when literal reference is not known
   * @param { string } display - text alternative for the resource
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public addHasMember(
    reference?: string,
    identifier?: Identifier,
    display?: string
  ): ObservationBuilder {
    const hasMember: Reference = {} as Reference
    hasMember.reference = reference
    if (typeof hasMember.reference === 'undefined') {
      hasMember.identifier = identifier
    }
    hasMember.display = display
    if (!Array.isArray(this.observation.hasMember)) {
      this.observation.hasMember = []
    }
    this.observation.hasMember.push(hasMember)
    return this
  }

  /**
   * Removes all of the related resources that was added as a hasMember.
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeAllHasMembers(): boolean {
    delete this.observation.hasMember
    return true
  }

  /**
   * Removes one of the related resource that was added as a hasMember, identified by a literal or logical reference.
   * A literal reference is preferred, but if a literal reference is not known, the logical reference is used.
   * @param { string } reference - literal reference, Relative, internal or absolute URL
   * @param { Identifier } identifier - logical reference, when literal reference is not known
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeHasMember(reference?: string, identifier?: Identifier): boolean {
    let returnValue = false
    if (typeof reference !== 'undefined') {
      this.observation.hasMember.forEach((hasMember, index) => {
        if (hasMember.reference === reference) {
          this.observation.hasMember.splice(index, 1)
          returnValue = true
        }
      })
    } else if (typeof identifier !== 'undefined') {
      this.observation.hasMember.forEach((hasMember, index) => {
        if (hasMember.identifier === identifier) {
          this.observation.hasMember.splice(index, 1)
          returnValue = true
        }
      })
    }
    return returnValue
  }

  /**
   * Returns all components for observation
   * @returns { ObservationComponent[] } - the components
   */
  public getComponents(): ObservationComponent[] {
    if (Array.isArray(this.observation.component)) {
      return this.observation.component
    }
    return []
  }

  /**
   * Returns a component with a specific system and code
   * @param { uri } system - The system
   * @param { code } code - The code
   * @returns { ObservationComponent } The component with the system and code
   */
  public getComponent(system: uri, theCode: string): ObservationComponent {
    let returnComponent
    if (this.observation.component === undefined) {
      return null
    }
    this.observation.component.forEach(component => {
      if (
        component.code &&
        component.code.coding &&
        Array.isArray(component.code.coding)
      ) {
        component.code.coding.forEach(code => {
          if (code.system === system && code.code === theCode) {
            returnComponent = component
          }
        })
      }
    })
    return returnComponent
  }

  /**
   * Replaces the component array with the provided array
   * @param { ObservationComponent[] } components - the component array to be set
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public setComponents(components: ObservationComponent[]): ObservationBuilder {
    const newComponents: ObservationComponent[] = []
    components.forEach(component => {
      try {
        this.addComponent(component)
      } catch (error) {
        throw error
      }
    })
    return this
  }

  /**
   * Appends a component to the observation
   * @param { ObservationComponent } component - the component to be appended
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public addComponent(component: ObservationComponent): ObservationBuilder {
    if (
      component.code &&
      component.code.coding &&
      Array.isArray(component.code.coding)
    ) {
      component.code.coding.forEach(code => {
        if (this.getComponent(code.system, code.code) != null) {
          throw new Error(
            `The provided observation already contains a component,
             which has one or more of the same codes as the provided component.`
          )
        }
      })
    }
    if (Array.isArray(this.observation.component)) {
      this.observation.component.push(component)
    } else {
      this.observation.component = [component]
    }
    return this
  }

  /**
   * Removes all components from the observation
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeComponents(): boolean {
    delete this.observation.component
    return false
  }

  /**
   * Sets an existing observation for this ObservationBuilder to allow for manipulating, or interacting with, the
   * observation through this ObservationBuilder
   * @param { Observation } observation - The observation to set
   * @returns { ObservationBuilder } This ObservationBuilder
   */
  public setObservation(observation: Observation): ObservationBuilder {
    this.observation = observation
    return this
  }

  /**
   * Create a FHIR Observation resource from a FHIR ObservationDefinition resource, and sets it in the observationbuilder
   * @param { IObservationDefinition } observationDefinition - the FHIR ObservationDefinition resource to create the observation from
   * @returns { ObservationBuilder } - this ObservationBuilder
   */
  public observationFromDefinition(
    observationDefinition: ObservationDefinition
  ): ObservationBuilder {
    const observation: Observation = this.make()

    // category
    if (observationDefinition.category) {
      const cat = observationDefinition.category as CodeableConcept[]
      observation.category = cat
    }

    // code
    if (observationDefinition.code) {
      const code = observationDefinition.code as CodeableConcept
      observation.code = code
    }

    // method
    if (observationDefinition.method) {
      observation.method = observationDefinition.method
    }

    // referencerange
    if (observationDefinition.qualifiedInterval) {
      observation.referenceRange = []
      observationDefinition.qualifiedInterval.forEach(qualifiedInterval => {
        const referenceRange: ObservationReferenceRange = {} as ObservationReferenceRange

        if (typeof qualifiedInterval.range !== 'undefined') {
          referenceRange.low = qualifiedInterval.range.low
          referenceRange.high = qualifiedInterval.range.low
        }

        referenceRange.age = qualifiedInterval.age
        referenceRange.appliesTo = qualifiedInterval.appliesTo
        referenceRange.type = qualifiedInterval.type

        observation.referenceRange.push(referenceRange)
      })
    }

    // canonical reference
    if (observationDefinition.extension && observationDefinition.extension.length !== 0) {
      const urlExtension = observationDefinition.extension.find(ext => ext.url === 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/186744833/ObservationDefinition+url+extension')
      const versionExtension = observationDefinition.extension.find(ext => ext.url === 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/197066753/ObservationDefinition+version+extension')

      if (urlExtension && urlExtension.valueUri) {
        const canonicalExtension = {
          url: 'https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110723073/Observation+definition+extension',
          valueCanonical: `${urlExtension.valueUri}${versionExtension && versionExtension.valueString ? '|' + versionExtension.valueString : ''}`
        }
        observation.extension = observation.extension || []
        observation.extension.push(canonicalExtension)
      }
    }

    this.observation = observation
    return this
  }

  /**
   * Checks if value[x] is set for the observation
   * @returns { boolean } - a boolean value indication whether the value is set
   */
  private checkIfValueIsSet(): boolean {
    if (
      typeof this.observation.valueBoolean !== 'undefined' ||
      typeof this.observation.valueCodeableConcept !== 'undefined' ||
      typeof this.observation.valueDateTime !== 'undefined' ||
      typeof this.observation.valueInteger !== 'undefined' ||
      typeof this.observation.valuePeriod !== 'undefined' ||
      typeof this.observation.valueQuantity !== 'undefined' ||
      typeof this.observation.valueRange !== 'undefined' ||
      typeof this.observation.valueRatio !== 'undefined' ||
      typeof this.observation.valueSampledData !== 'undefined' ||
      typeof this.observation.valueString !== 'undefined' ||
      typeof this.observation.valueTime !== 'undefined'
    ) {
      return true
    }
    return false
  }

  /**
   * Checks if effective[x] is set for the observation
   * @returns { boolean } - a boolean value indication whether the effective[x] is set
   */
  private checkIfEffectiveSet(): boolean {
    if (
      typeof this.observation.effectiveDateTime !== 'undefined' ||
      typeof this.observation.effectiveInstant !== 'undefined' ||
      typeof this.observation.effectivePeriod !== 'undefined' ||
      typeof this.observation.effectiveTiming !== 'undefined'
    ) {
      return true
    }
    return false
  }

  /**
   * Make a new object that implements a FHIR Observation
   * @returns { Observation } - an object that implements FHIR Observation
   */
  private make(): Observation {
    const observation: Observation = {
      resourceType: 'Observation',
      status: 'registered',
      identifier: [
        {
          system: 'urn:ietf:rfc:3986',
          value: Guid.newGuid(),
        },
      ],
    } as Observation

    return observation
  }

  /**
   * Builds and returns the observation
   * @returns { Observation } - the observation
   */
  public build(): Observation {
    return this.observation
  }
}
