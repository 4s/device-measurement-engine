import { Quantity, CodeableConcept, decimal, code, uri, dateTime, integer, Period, Range, Ratio, positiveInt, SampledData, time, ObservationComponent, QuantityComparator } from 'fhir';
/** Class representing a FHIR ObservationComponent builder */
export declare class ObservationComponentBuilder {
    /**
     * The component being built
     * @private
     * @type { ObservationComponent }
     * @ignore
     */
    protected component: ObservationComponent;
    /**
     * Creates a new FHIR ObservationComponentBuilder
     */
    constructor();
    /**
     * Sets the code for the type of component
     * @param { string } text - Plain text representation of the type of component
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setCode(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): ObservationComponentBuilder;
    /**
     * Removes the code for the type of component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeCode(): boolean;
    /**
     * Returns the value from the component
     * @returns { boolean | dateTime | integer | string | time | Period | Quantity | CodeableConcept | Range | Ratio | SampledData } - The value
     */
    getValue(): boolean | dateTime | integer | string | time | Period | Quantity | CodeableConcept | Range | Ratio | SampledData;
    /**
     * Set the value of the component as a boolean, if no value has been set on the component.
     * @param { boolean } value - the value to be set
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueBoolean(value: boolean): ObservationComponentBuilder;
    /**
     * Removes valueBoolean from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueBoolean(): boolean;
    /**
     * Set the value of the component as a dateTime, if no value has been set on the component.
     * @param { dateTime } value - the value to be set
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueDateTime(value: dateTime): ObservationComponentBuilder;
    /**
     * Removes valueDateTime from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueDateTime(): boolean;
    /**
     * Set the value of the component as a integer, if no value has been set on the component.
     * @param { integer } value - the value to be set
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueInteger(value: integer): ObservationComponentBuilder;
    /**
     * Removes valueInteger from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueInteger(): boolean;
    /**
     * Set the value of the component as a string, if no value has been set on the component.
     * @param { string } value - the value to be set
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueString(value: string): ObservationComponentBuilder;
    /**
     * Removes valueString from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueString(): boolean;
    /**
     * Set the value of the component as a time, if no value has been set on the component.
     * @param { time } value - the value to be set
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueTime(value: time): ObservationComponentBuilder;
    /**
     * Removes valueTime from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueTime(): boolean;
    /**
     * Set the value of the component as a Period, if no value has been set on the component.
     * If either start or end is left out, the period is considered open-ended.
     * Both should not be left out simoultaneously.
     * @param { dateTime } start - the start dateTime of the period
     * @param { dateTime } end - the end dateTime of the period
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValuePeriod(start?: dateTime, end?: dateTime): ObservationComponentBuilder;
    /**
     * Removes valuePeriod from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValuePeriod(): boolean;
    /**
     * Set the value of the component as a Quantity with a value and a unit in a specific coding system, if no value
     * has been set on the component.
     * @param { decimal } value - Numerical value (with implicit precision)
     * @param { code } comparator - < | <= | >= | > - how to understand the value
     * @param { string } unit - Unit representation
     * @param { uri } system - System that defines coded unit form
     * @param { code } code - Coded form of the unit
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueQuantity(value?: decimal, comparator?: QuantityComparator, unit?: string, system?: uri, code?: code): ObservationComponentBuilder;
    /**
     * Removes valueQuantity from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueQuantity(): boolean;
    /**
     * Set the value of the component as a CodeableConcept, if no value has been set on the component.
     * @param { string } text - Plain text representation of the concept
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueCodeableConcept(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): ObservationComponentBuilder;
    /**
     * Removes valueCodeableConcept from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueCodeableConcept(): boolean;
    /**
     * Set the value of the component as a Range between two values, if no value has been set on the component.
     * Whether to provide a specific coding system is optional.
     * @param { decimal } lowValue - The lower value of the range
     * @param { decimal } highValue - The high value of the range
     * @param { string } unit - Unit representation
     * @param { uri } system - System that defines coded unit form
     * @param { code } code - Coded form of the unit
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueRange(lowValue?: decimal, highValue?: decimal, unit?: string, system?: uri, code?: code): ObservationComponentBuilder;
    /**
     * Removes valueRange from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueRange(): boolean;
    /**
     * Set the value of the component as a Ratio, if no value has been set on the component.
     * Whether to provide a specific coding system is optional.
     * @param { decimal } numerator - The numerator in the ratio
     * @param { decimal } denominator - The denominator in the ratio
     * @param { string } unit - Unit representation
     * @param { uri } system - System that defines coded unit form
     * @param { code } code - Coded form of the unit
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueRatio(numerator?: decimal, denominator?: decimal, unit?: string, system?: uri, code?: code): ObservationComponentBuilder;
    /**
     * Removes valueRatio from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueRatio(): boolean;
    /**
     * Set the value of the component as a collection of SampledData, if no value has been set on the component.
     * @param { decimal } originValue - The zero value
     * @param { string } originUnit - Unit representation of the zero value
     * @param { uri } originSystem - System that defines coded unit form of the zero value
     * @param { code } originCode - Coded form of the unit of the zero value
     * @param { decimal } period - Number of milliseconds between samples
     * @param { positiveInt } dimensions - Number of sample points at each time point
     * @param { string } data - Decimal values with spaces, or "E" | "U" | "L"
     * @param { decimal } factor - Multiply data by this before adding to origin
     * @param { decimal } lowerLimit - Lower limit of detection
     * @param { decimal } upperLimit - Upper limit of detection
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setValueSampledData(originValue: decimal, originUnit: string, originSystem: uri, originCode: code, period: decimal, dimensions: positiveInt, data: string, factor?: decimal, lowerLimit?: decimal, upperLimit?: decimal): ObservationComponentBuilder;
    /**
     * Removes valueSampledData from the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeValueSampledData(): boolean;
    /**
     * Sets the reason for the data being absent
     * @param { string } text - Plain text representation of the reason for the data being absent
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setDataAbsentReason(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): ObservationComponentBuilder;
    /**
     * Removes the reason for the data being absent
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeDataAbsentReason(): boolean;
    /**
     * Sets the interpretation for the component
     * @param { string } text - Plain text representation of the interpretation for the component
     * @param { uri } system - Identity of the terminology system
     * @param { string } version - Contains extended information for property 'system'
     * @param { code } code - Symbol in syntax defined by the system
     * @param { string } display - Representation defined by the system
     * @param { boolean } userSelected - If this coding was chosen directly by the user
     * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
     */
    setInterpretation(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): ObservationComponentBuilder;
    /**
     * Removes the interpretation for the component
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeInterpretation(): boolean;
    /**
     * Sets an existing component for this ObservationComponentBuilder to allow for manipulating the component through this
     * ObservationComponentBuilder
     * @param { ObservationComponent } component - The component to set
     * @returns { ObservationComponentBuilder } this ObservationComponentBuilder
     */
    setComponent(component: ObservationComponent): void;
    /**
     * Checks if value[x] is set for the component
     * @returns { boolean } - a boolean value indication whether the value is set
     */
    private checkIfValueIsSet;
    /**
     * Make a new object that implements a FHIR ObservationComponent
     * @returns { ObservationComponent } - an object that implements FHIR ObservationComponent
     */
    private make;
    /**
     * Builds and returns the component
     * @returns { ObservationComponent } - the component
     */
    build(): ObservationComponent;
}
