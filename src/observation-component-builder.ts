
import {
  Quantity,
  CodeableConcept,
  decimal,
  code,
  uri,
  Coding,
  dateTime,
  integer,
  Period,
  Range,
  Ratio,
  positiveInt,
  SampledData,
  time,
  ObservationComponent,
  QuantityComparator,
} from 'fhir'

/** Class representing a FHIR ObservationComponent builder */
export class ObservationComponentBuilder {
  /**
   * The component being built
   * @private
   * @type { ObservationComponent }
   * @ignore
   */
  protected component: ObservationComponent

  /**
   * Creates a new FHIR ObservationComponentBuilder
   */
  public constructor() {
    this.component = this.make()
  }

  /**
   * Sets the code for the type of component
   * @param { string } text - Plain text representation of the type of component
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setCode(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): ObservationComponentBuilder {
    const codeableConcept: CodeableConcept = {
      coding: [
        {
          system,
          version,
          code,
          display,
          userSelected,
        },
      ],
      text,
    } as CodeableConcept
    this.component.code = codeableConcept
    return this
  }

  /**
   * Removes the code for the type of component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeCode(): boolean {
    delete this.component.code
    return true
  }

  /**
   * Returns the value from the component
   * @returns { boolean | dateTime | integer | string | time | Period | Quantity | CodeableConcept | Range | Ratio | SampledData } - The value
   */
  public getValue():
    | boolean
    | dateTime
    | integer
    | string
    | time
    | Period
    | Quantity
    | CodeableConcept
    | Range
    | Ratio
    | SampledData {
    if (this.component.valueBoolean != null) {
      return this.component.valueBoolean
    } else if (this.component.valueCodeableConcept != null) {
      this.component.valueCodeableConcept
    } else if (this.component.valueDateTime != null) {
      this.component.valueDateTime
    } else if (this.component.valueInteger != null) {
      this.component.valueInteger
    } else if (this.component.valuePeriod != null) {
      this.component.valuePeriod
    } else if (this.component.valueQuantity != null) {
      this.component.valueQuantity
    } else if (this.component.valueRange != null) {
      this.component.valueRange
    } else if (this.component.valueRatio != null) {
      this.component.valueRatio
    } else if (this.component.valueSampledData != null) {
      this.component.valueSampledData
    } else if (this.component.valueString != null) {
      this.component.valueString
    } else if (this.component.valueTime != null) {
      this.component.valueTime
    } else {
      return null
    }
  }

  /**
   * Set the value of the component as a boolean, if no value has been set on the component.
   * @param { boolean } value - the value to be set
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueBoolean(value: boolean): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueBoolean. Value is already set for component.'
      )
    }
    this.component.valueBoolean = value

    return this
  }

  /**
   * Removes valueBoolean from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueBoolean(): boolean {
    delete this.component.valueBoolean
    return true
  }

  /**
   * Set the value of the component as a dateTime, if no value has been set on the component.
   * @param { dateTime } value - the value to be set
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueDateTime(value: dateTime): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueDateTime. Value is already set for component.'
      )
    }
    this.component.valueDateTime = value

    return this
  }

  /**
   * Removes valueDateTime from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueDateTime(): boolean {
    delete this.component.valueDateTime
    return true
  }

  /**
   * Set the value of the component as a integer, if no value has been set on the component.
   * @param { integer } value - the value to be set
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueInteger(value: integer): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueInteger. Value is already set for component.'
      )
    }
    this.component.valueInteger = value

    return this
  }

  /**
   * Removes valueInteger from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueInteger(): boolean {
    delete this.component.valueInteger
    return true
  }

  /**
   * Set the value of the component as a string, if no value has been set on the component.
   * @param { string } value - the value to be set
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueString(value: string): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueString. Value is already set for component.'
      )
    }
    this.component.valueString = value

    return this
  }

  /**
   * Removes valueString from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueString(): boolean {
    delete this.component.valueString
    return true
  }

  /**
   * Set the value of the component as a time, if no value has been set on the component.
   * @param { time } value - the value to be set
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueTime(value: time): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueTime. Value is already set for component.'
      )
    }
    this.component.valueTime = value

    return this
  }

  /**
   * Removes valueTime from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueTime(): boolean {
    delete this.component.valueTime
    return true
  }

  /**
   * Set the value of the component as a Period, if no value has been set on the component.
   * If either start or end is left out, the period is considered open-ended.
   * Both should not be left out simoultaneously.
   * @param { dateTime } start - the start dateTime of the period
   * @param { dateTime } end - the end dateTime of the period
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValuePeriod(
    start?: dateTime,
    end?: dateTime
  ): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valuePeriod. Value is already set for component.'
      )
    }
    this.component.valuePeriod = {
      start,
      end,
    } as Period

    return this
  }

  /**
   * Removes valuePeriod from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValuePeriod(): boolean {
    delete this.component.valuePeriod
    return true
  }

  /**
   * Set the value of the component as a Quantity with a value and a unit in a specific coding system, if no value
   * has been set on the component.
   * @param { decimal } value - Numerical value (with implicit precision)
   * @param { code } comparator - < | <= | >= | > - how to understand the value
   * @param { string } unit - Unit representation
   * @param { uri } system - System that defines coded unit form
   * @param { code } code - Coded form of the unit
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueQuantity(
    value?: decimal,
    comparator?: QuantityComparator,
    unit?: string,
    system?: uri,
    code?: code
  ): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueQuantity. Value is already set for component.'
      )
    }
    const valueQuantity: Quantity = {} as Quantity

    valueQuantity.value = value
    valueQuantity.comparator = comparator
    valueQuantity.unit = unit
    valueQuantity.system = system
    valueQuantity.code = code

    this.component.valueQuantity = valueQuantity
    return this
  }

  /**
   * Removes valueQuantity from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueQuantity(): boolean {
    delete this.component.valueQuantity
    return true
  }

  /**
   * Set the value of the component as a CodeableConcept, if no value has been set on the component.
   * @param { string } text - Plain text representation of the concept
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueCodeableConcept(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueCodeableConcept. Value is already set for component.'
      )
    }
    const codeableConcept: CodeableConcept = {} as CodeableConcept
    const coding: Coding = {} as Coding

    coding.system = system
    coding.version = version
    coding.code = code
    coding.display = display
    coding.userSelected = userSelected

    codeableConcept.coding = [coding]
    codeableConcept.text = text

    this.component.valueCodeableConcept = codeableConcept
    return this
  }

  /**
   * Removes valueCodeableConcept from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueCodeableConcept(): boolean {
    delete this.component.valueCodeableConcept
    return true
  }

  /**
   * Set the value of the component as a Range between two values, if no value has been set on the component.
   * Whether to provide a specific coding system is optional.
   * @param { decimal } lowValue - The lower value of the range
   * @param { decimal } highValue - The high value of the range
   * @param { string } unit - Unit representation
   * @param { uri } system - System that defines coded unit form
   * @param { code } code - Coded form of the unit
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueRange(
    lowValue?: decimal,
    highValue?: decimal,
    unit?: string,
    system?: uri,
    code?: code
  ): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueRange. Value is already set for component.'
      )
    }
    const lowQuantity: Quantity = {
      value: lowValue,
      unit,
      system,
      code,
    } as Quantity

    const highQuantity: Quantity = {
      value: highValue,
      unit,
      system,
      code,
    } as Quantity

    const range: Range = {
      low: lowQuantity,
      high: highQuantity,
    } as Range

    this.component.valueRange = range

    return this
  }

  /**
   * Removes valueRange from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueRange(): boolean {
    delete this.component.valueRange
    return true
  }

  /**
   * Set the value of the component as a Ratio, if no value has been set on the component.
   * Whether to provide a specific coding system is optional.
   * @param { decimal } numerator - The numerator in the ratio
   * @param { decimal } denominator - The denominator in the ratio
   * @param { string } unit - Unit representation
   * @param { uri } system - System that defines coded unit form
   * @param { code } code - Coded form of the unit
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueRatio(
    numerator?: decimal,
    denominator?: decimal,
    unit?: string,
    system?: uri,
    code?: code
  ): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueRatio. Value is already set for component.'
      )
    }
    const numeratorQuantity: Quantity = {
      value: numerator,
      unit,
      system,
      code,
    } as Quantity

    const denominatorQuantity: Quantity = {
      value: denominator,
      unit,
      system,
      code,
    } as Quantity

    const ratio: Ratio = {
      numerator: numeratorQuantity,
      denominator: denominatorQuantity,
    } as Ratio

    this.component.valueRatio = ratio

    return this
  }

  /**
   * Removes valueRatio from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueRatio(): boolean {
    delete this.component.valueRatio
    return true
  }

  /**
   * Set the value of the component as a collection of SampledData, if no value has been set on the component.
   * @param { decimal } originValue - The zero value
   * @param { string } originUnit - Unit representation of the zero value
   * @param { uri } originSystem - System that defines coded unit form of the zero value
   * @param { code } originCode - Coded form of the unit of the zero value
   * @param { decimal } period - Number of milliseconds between samples
   * @param { positiveInt } dimensions - Number of sample points at each time point
   * @param { string } data - Decimal values with spaces, or "E" | "U" | "L"
   * @param { decimal } factor - Multiply data by this before adding to origin
   * @param { decimal } lowerLimit - Lower limit of detection
   * @param { decimal } upperLimit - Upper limit of detection
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setValueSampledData(
    originValue: decimal,
    originUnit: string,
    originSystem: uri,
    originCode: code,
    period: decimal,
    dimensions: positiveInt,
    data: string,
    factor?: decimal,
    lowerLimit?: decimal,
    upperLimit?: decimal
  ): ObservationComponentBuilder {
    if (this.checkIfValueIsSet()) {
      throw new Error(
        'Cannot set valueSampledData. Value is already set for component.'
      )
    }
    const originQuantity: Quantity = {
      value: originValue,
      unit: originUnit,
      system: originSystem,
      code: originCode,
    } as Quantity

    const sampledData = {
      origin: originQuantity,
      period,
      factor,
      lowerLimit,
      upperLimit,
      dimensions,
      data,
    } as SampledData

    this.component.valueSampledData = sampledData

    return this
  }

  /**
   * Removes valueSampledData from the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeValueSampledData(): boolean {
    delete this.component.valueSampledData
    return true
  }

  /**
   * Sets the reason for the data being absent
   * @param { string } text - Plain text representation of the reason for the data being absent
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setDataAbsentReason(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): ObservationComponentBuilder {
    const codeableConcept: CodeableConcept = {
      coding: [
        {
          system,
          version,
          code,
          display,
          userSelected,
        },
      ],
      text,
    } as CodeableConcept
    this.component.dataAbsentReason = codeableConcept
    return this
  }

  /**
   * Removes the reason for the data being absent
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeDataAbsentReason(): boolean {
    delete this.component.dataAbsentReason
    return true
  }

  /**
   * Sets the interpretation for the component
   * @param { string } text - Plain text representation of the interpretation for the component
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { ObservationComponentBuilder } - this ObservationComponentBuilder
   */
  public setInterpretation(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): ObservationComponentBuilder {
    const codeableConcept: CodeableConcept = {
      coding: [
        {
          system,
          version,
          code,
          display,
          userSelected,
        },
      ],
      text,
    } as CodeableConcept
    this.component.interpretation = codeableConcept
    return this
  }

  /**
   * Removes the interpretation for the component
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeInterpretation(): boolean {
    delete this.component.interpretation
    return true
  }

  /**
   * Sets an existing component for this ObservationComponentBuilder to allow for manipulating the component through this
   * ObservationComponentBuilder
   * @param { ObservationComponent } component - The component to set
   * @returns { ObservationComponentBuilder } this ObservationComponentBuilder
   */
  public setComponent(component: ObservationComponent) {
    this.component = component
  }

  /**
   * Checks if value[x] is set for the component
   * @returns { boolean } - a boolean value indication whether the value is set
   */
  private checkIfValueIsSet(): boolean {
    if (
      typeof this.component.valueBoolean !== 'undefined' ||
      typeof this.component.valueCodeableConcept !== 'undefined' ||
      typeof this.component.valueDateTime !== 'undefined' ||
      typeof this.component.valueInteger !== 'undefined' ||
      typeof this.component.valuePeriod !== 'undefined' ||
      typeof this.component.valueQuantity !== 'undefined' ||
      typeof this.component.valueRange !== 'undefined' ||
      typeof this.component.valueRatio !== 'undefined' ||
      typeof this.component.valueSampledData !== 'undefined' ||
      typeof this.component.valueString !== 'undefined' ||
      typeof this.component.valueTime !== 'undefined'
    ) {
      return true
    }
    return false
  }
  /**
   * Make a new object that implements a FHIR ObservationComponent
   * @returns { ObservationComponent } - an object that implements FHIR ObservationComponent
   */
  private make(): ObservationComponent {
    const component: ObservationComponent = {} as ObservationComponent

    return component
  }

  /**
   * Builds and returns the component
   * @returns { ObservationComponent } - the component
   */
  public build(): ObservationComponent {
    return this.component
  }
}
