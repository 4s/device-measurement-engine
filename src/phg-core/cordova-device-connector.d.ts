import { DeviceConnector } from './device-connector';
import { Device, Observation } from 'fhir';
/** Class representing a DeviceConnector for Cordova */
export declare class CordovaDeviceConnector implements DeviceConnector {
    /**
     * Object from which to access KomtelPlugin
     * @private
     * @type { any }
     * @ignore
     */
    private win;
    /**
     * Whether or not the device is bonded
     * @private
     * @type { boolean }
     * @ignore
     */
    private bonded;
    /**
     * The address of the device
     * @private
     * @type { string }
     * @ignore
     */
    private address;
    /**
     * The name of the device
     * @private
     * @type { string }
     * @ignore
     */
    private name;
    /**
     * Creates a new CordovaDeviceConnector
     * @param { string } name - The name of the device
     * @param { string } address - The address of the device
     * @param { boolean } isBonded - Whether the device is bonded or not
     */
    constructor(name: string, address: string, isBonded: boolean);
    /**
     * Returns whether or not the device is bonded
     * @returns boolean
     */
    isBonded(): boolean;
    /**
     * Bonds with the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    bond(): Promise<boolean>;
    /**
     * Unbonds from the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    unbond(): Promise<boolean>;
    /**
     * Starts a measurement
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    startMeasurement(): Promise<boolean>;
    /**
     * Stops the running measurement
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    stopMeasurement(): Promise<boolean>;
    /**
     * Retrieves the latest measurement as a FHIR Observation
     * @returns { Promise<Observation> } A promise resolving to a FHIR Observation
     */
    getDeviceAndMeasurement(): Promise<{
        device: Device;
        measurement: Observation;
    }>;
}
