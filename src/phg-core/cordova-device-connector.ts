
import { DeviceConnector } from './device-connector'
import { Device, Observation } from 'fhir'

/** Class representing a DeviceConnector for Cordova */
export class CordovaDeviceConnector implements DeviceConnector {
  /**
   * Object from which to access KomtelPlugin
   * @private
   * @type { any }
   * @ignore
   */
  private win: any = window as any
  /**
   * Whether or not the device is bonded
   * @private
   * @type { boolean }
   * @ignore
   */
  private bonded: boolean
  /**
   * The address of the device
   * @private
   * @type { string }
   * @ignore
   */
  private address: string
  /**
   * The name of the device
   * @private
   * @type { string }
   * @ignore
   */
  private name: string

  /**
   * Creates a new CordovaDeviceConnector
   * @param { string } name - The name of the device
   * @param { string } address - The address of the device
   * @param { boolean } isBonded - Whether the device is bonded or not
   */
  public constructor(name: string, address: string, isBonded: boolean) {
    this.bonded = isBonded
    this.name = name
    this.address = address
  }

  /**
   * Returns whether or not the device is bonded
   * @returns boolean
   */
  public isBonded(): boolean {
    return this.bonded
  }

  /**
   * Bonds with the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public bond(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this.bonded) {
        reject(new Error('Cannot bond with device. Device already bonded'))
      }
      this.win.komtelplugin.bond(
        this.address,
        (result: string) => {
          if (result === 'true') {
            resolve(true)
          }
          throw new Error('Something went wrong while bonding with the device')
        },
        (error: string) => {
          reject(new Error(error))
        }
      )
    })
  }
  /**
   * Unbonds from the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public unbond(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.bonded) {
        reject(new Error('Cannot unbond with device. Device not bonded'))
      }
      this.win.komtelplugin.unbond(
        this.address,
        (result: string) => {
          if (result === 'true') {
            resolve(true)
          }
          throw new Error('Something went wrong while unbonding from device')
        },
        (error: string) => {
          reject(new Error(error))
        }
      )
    })
  }

  /**
   * Starts a measurement
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public startMeasurement(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.win.komtelplugin.startMeasurement(
        this.address,
        (result: string) => {
          if (typeof result !== 'undefined') {
            resolve(true)
          }
          throw new Error('Something went wrong while stopping the measurement')
        },
        (error: string) => {
          reject(new Error(error))
        }
      )
    })
  }

  /**
   * Stops the running measurement
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public stopMeasurement(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.win.komtelplugin.stopMeasurement(
        this.address,
        (result: string) => {
          if (result === 'true') {
            resolve(true)
          }
          reject(
            new Error('Something went wrong while stopping the measurement')
          )
        },
        (error: string) => {
          reject(new Error(error))
        }
      )
    })
  }

  /**
   * Retrieves the latest measurement as a FHIR Observation
   * @returns { Promise<Observation> } A promise resolving to a FHIR Observation
   */
  public getDeviceAndMeasurement(): Promise<{ device: Device, measurement: Observation }> {
    return new Promise<{ device: Device, measurement: Observation }>((resolve, reject) => {
      this.win.komtelplugin.getDevice(
        this.address,
        (result: string) => {
          const device: Device = JSON.parse(result)
          this.win.komtelplugin.getMeasurement(
            this.address,
            (result: string) => {
              try {
                const measurement: Observation = JSON.parse(result)
                resolve({ device, measurement })
              } catch (error) {
                reject(error)
              }
            },
            (error: string) => {
              reject(new Error(error))
            }
          )
        },
        (error: string) => {
          reject(new Error(error))
        }
      )
    })
  }
}
