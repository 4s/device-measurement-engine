import { DeviceManager } from './device-manager';
import { DeviceConnector } from './device-connector';
import { code } from 'fhir';
/** A class representing a DeviceManager using a CordovaPlugin */
export declare class CordovaDeviceManager implements DeviceManager {
    /**
     * Object from which to access KomtelPlugin
     * @private
     * @type { any }
     * @ignore
     */
    private win;
    /**
     * Returns all currently available devices of the types provided as codes
     * @param { codes } code[] - The MDC-codes of the type of devices
     * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
     */
    getAvailableDevices(codes: code[]): Promise<DeviceConnector[]>;
    /**
     * Searches for devices of of the types provided as codes
     * @param { codes } code[] - The MDC-codes of the type of devices
     * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
     */
    searchForDevices(codes: code[]): Promise<DeviceConnector[]>;
}
