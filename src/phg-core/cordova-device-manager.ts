import { DeviceManager } from './device-manager'
import { DeviceConnector } from './device-connector'
import { code } from 'fhir'
import { CordovaDeviceConnector } from './cordova-device-connector'

/** A class representing a DeviceManager using a CordovaPlugin */
export class CordovaDeviceManager implements DeviceManager {
  /**
   * Object from which to access KomtelPlugin
   * @private
   * @type { any }
   * @ignore
   */
  private win: any = window as any

  /**
   * Returns all currently available devices of the types provided as codes
   * @param { codes } code[] - The MDC-codes of the type of devices
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
   */
  public getAvailableDevices(codes: code[]): Promise<DeviceConnector[]> {
    return new Promise<any>((resolve, reject) => {
      this.win.komtelplugin.getAvailableDevices(
        codes,
        (addresses: string) => {
          try {
            const deviceObjects: any[] = JSON.parse(addresses)
            const devices: DeviceConnector[] = [] as DeviceConnector[]
            deviceObjects.forEach(device => {
              devices.push(
                new CordovaDeviceConnector(
                  device.name,
                  device.address,
                  device.isBonded
                )
              )
            })
            resolve(devices)
          } catch (error) {
            reject(error)
          }
        },
        (error: string) => {
          reject(new Error(error))
        }
      )
    })
  }

  /**
   * Searches for devices of of the types provided as codes
   * @param { codes } code[] - The MDC-codes of the type of devices
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
   */
  public searchForDevices(codes: code[]): Promise<DeviceConnector[]> {
    return new Promise<any>((resolve, reject) => {
      this.win.komtelplugin.search(
        codes,
        (addresses: string) => {
          try {
            const deviceObjects: any[] = JSON.parse(addresses)
            let devices: DeviceConnector[]
            deviceObjects.forEach(device => {
              devices.push(
                new CordovaDeviceConnector(
                  device.name,
                  device.address,
                  device.isBonded
                )
              )
            })
            resolve(devices)
          } catch (error) {
            reject(error)
          }
        },
        (error: string) => {
          reject(new Error(error))
        }
      )
    })
  }
}
