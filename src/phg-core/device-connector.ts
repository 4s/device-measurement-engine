
import { Device, Observation } from 'fhir'

/** A class representing an interface for DeviceConnectors */
export interface DeviceConnector {
  /**
   * Returns whether the device is bonded or not
   * @returns { boolean } a boolean value to indicate whether the device is bonded or not
   */
  isBonded(): boolean
  /**
   * Bonds with the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  bond(): Promise<boolean>
  /**
   * Unbonds from the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  unbond(): Promise<boolean>
  /**
   * Retrieves the latest measurement
   * @returns { Promise<{ device: Device, measurement: Observation }> } A promise resolving to the latest measurement
   */
  getDeviceAndMeasurement(): Promise<{ device: Device, measurement: Observation }>
}
