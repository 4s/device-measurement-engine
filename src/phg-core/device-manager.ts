
import { code } from 'fhir'
import { DeviceConnector } from './device-connector'

/** A class representing an interface for DeviceManagers */
export interface DeviceManager {
  /**
   * Returns all currently available devices of the types provided as codes
   * @param { codes } code[] - The MDC-codes of the type of devices
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
   */
  getAvailableDevices(codes: code[]): Promise<DeviceConnector[]>
  /**
   * Searches for devices of of the types provided as codes
   * @param { codes } code[] - The MDC-codes of the type of devices
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
   */
  searchForDevices(codes: code[]): Promise<DeviceConnector[]>
}
