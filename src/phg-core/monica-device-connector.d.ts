import { DeviceConnector } from './device-connector';
import { Device, Observation } from 'fhir';
import { InterfaceEndpoint, Baseplate } from 'phg-js-baseplate';
export declare class MonicaDeviceConnector implements DeviceConnector {
    private measurementConsumer;
    private monicaConsumer;
    private device;
    private observation;
    private statusMessage;
    private statusCallback;
    constructor(baseplate: Baseplate);
    /**
     * Returns whether or not the device is bonded
     * @returns boolean
     */
    isBonded(): boolean;
    /**
     * Bonds with the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    bond(): Promise<boolean>;
    /**
     * Unbonds from the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    unbond(): Promise<boolean>;
    /**
     * Starts a measurement
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    startMeasurement(): Promise<boolean>;
    /**
     * Stops the running measurement
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    stopMeasurement(): Promise<boolean>;
    /**
     * Retrieves the latest measurement
     * @returns { Promise<Observation> } A promise resolving to the latest measurement
     */
    getDeviceAndMeasurement(): Promise<{
        device: Device;
        measurement: Observation;
    }>;
    /**
     * Returns current  status
     */
    readonly status: string;
    /**
     * Subscribes to status updates with the provided callback method
     * @param callback The method to be called when a statusmessage is received
     */
    subscribeToStatus(callback: (statusMessage: string) => void): void;
    /**
     * method to be called upon receiving an observation
     * @param measurementId id of the measurement
     * @param observation the observation
     * @param device the device
     * @param meta metadata
     */
    private measurementReceived;
    /**
     * method to be called upon receiving a statusmessage
     * @param currentStatus the currentStatus
     * @param meta metadata
     */
    private currentStatusReceived;
    /**
     * ONLY USED FOR TESTS
     */
    getMeasurementConsumer(): InterfaceEndpoint;
    /**
     * ONLY USED FOR TESTS
     */
    getMonicaConsumer(): InterfaceEndpoint;
}
