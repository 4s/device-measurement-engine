import { DeviceConnector } from './device-connector'
import { Device, Observation } from 'fhir'
import { MeasurementReceived } from 'phg-messages/types/FHIRMeasurement_pb'
import { CurrentStatus } from 'phg-messages/types/Monica_pb'
import { InterfaceEndpoint, MetaData, Baseplate, ModuleBase } from 'phg-js-baseplate'

export class MonicaDeviceConnector implements DeviceConnector {
  private measurementConsumer: InterfaceEndpoint
  private monicaConsumer: InterfaceEndpoint
  private device: Device
  private observation: Observation
  private statusMessage: string
  private statusCallback: (statusMessage: string) => void | undefined

  public constructor(baseplate: Baseplate) {
    const moduleBase: ModuleBase = new baseplate.ModuleBase("Monica-device-connector")
    this.statusMessage = 'No statusmessage received'
    try {
      this.measurementConsumer = moduleBase.implementsConsumerInterface("FHIRMeasurement")
      this.measurementConsumer.addEventHandler("MeasurementReceived",
        (input: MeasurementReceived, meta: MetaData) => {
          this.measurementReceived(input, meta)
        }
      )
    } catch (e) {
      throw new Error('error while initiating measurementConsumer: ' + e)
    }

    try {
      this.monicaConsumer = moduleBase.implementsConsumerInterface("Monica")
      this.monicaConsumer.addEventHandler("CurrentStatus",
        (input: CurrentStatus, meta: MetaData) => {
          this.currentStatusReceived(input, meta)
        }
      )
    } catch (e) {
      throw new Error('error while initiating monicaConsumer: ' + e)
    }
    moduleBase.start();
  }

  /**
   * Returns whether or not the device is bonded
   * @returns boolean
   */
  public isBonded(): boolean {
    throw new Error('function not implemented')
  }

  /**
   * Bonds with the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public bond(): Promise<boolean> {
    throw new Error('function not implemented')
  }

  /**
   * Unbonds from the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public unbond(): Promise<boolean> {
    throw new Error('function not implemented')
  }

  /**
   * Starts a measurement
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public startMeasurement(): Promise<boolean> {
    return new Promise<boolean>((resolve, _) => {
      this.monicaConsumer.postEvent("Start",
        (args: any) => { /* do nothing */ }
      )
      resolve(true)
    })

  }

  /**
   * Stops the running measurement
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public stopMeasurement(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.monicaConsumer.postEvent("Stop",
        (args: any) => { /* do nothing */ }
      )
      resolve(true)
    })
  }

  /**
   * Retrieves the latest measurement
   * @returns { Promise<Observation> } A promise resolving to the latest measurement
   */
  public getDeviceAndMeasurement(): Promise<{ device: Device, measurement: Observation }> {
    return new Promise<{ device: Device, measurement: Observation }>((resolve, reject) => {
      const wait = () => {
        if (this.observation && this.device) return resolve({ device: this.device, measurement: this.observation });
        setTimeout(wait.bind(this, resolve, reject), 2);
      }
      wait();
    });
  }

  /**
   * Returns current  status
   */
  public get status(): string {
    return this.statusMessage
  }

  /**
   * Subscribes to status updates with the provided callback method
   * @param callback The method to be called when a statusmessage is received
   */
  public subscribeToStatus(callback: (statusMessage: string) => void): void {
    this.statusCallback = callback
  }

  /**
   * method to be called upon receiving an observation
   * @param measurementId id of the measurement
   * @param observation the observation
   * @param device the device
   * @param meta metadata
   */
  private measurementReceived(measurement: MeasurementReceived, meta: MetaData) {
    const mdcCode = measurement.getDeviceType()
    const observation = measurement.getFhirObservation()
    const device = measurement.getFhirDevice()

    /* tslint:disable */
    console.log("measurementReceived(\"" + mdcCode + "\", \"" + observation + "\", \"" + device + "\", { " +
      ", sender: \"" + meta.sender + "\"})")
    /* tslint:enable */

    if (mdcCode !== 'MDC585728' && mdcCode !== '585728') {
      return
    }
    if (typeof observation !== "string" || typeof device !== "string") {
      throw new Error('Malformed json')
    }
    try {
      this.observation = JSON.parse(observation)
      this.device = JSON.parse(device)
    }
    catch (error) {
      throw error
    }
  }

  /**
   * method to be called upon receiving a statusmessage
   * @param currentStatus the currentStatus
   * @param meta metadata
   */
  private currentStatusReceived(currentStatus: CurrentStatus, meta: MetaData) {
    const statusMessage = currentStatus.getCurrentStatus();
    /* tslint:disable */
    console.log("statusMessageReceived(\"" + statusMessage + "\", sender: \"" + meta.sender + "\"})")
    /* tslint:enable */

    if (typeof statusMessage !== "string") {
      throw new Error('Malformed statusMessagae')
    }
    this.statusMessage = statusMessage
    this.statusCallback(statusMessage)
  }

  /**
   * ONLY USED FOR TESTS
   */
  getMeasurementConsumer() {
    return this.measurementConsumer
  }


  /**
   * ONLY USED FOR TESTS
   */
  getMonicaConsumer() {
    return this.monicaConsumer
  }
}