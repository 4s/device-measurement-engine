import { DeviceConnector } from './device-connector';
import { Device, Observation } from 'fhir';
import { Baseplate } from 'phg-js-baseplate';
export declare class PHGCoreDeviceConnector implements DeviceConnector {
    private consumer;
    private device;
    private observation;
    constructor(baseplate: Baseplate);
    /**
     * Returns whether or not the device is bonded
     * @returns boolean
     */
    isBonded(): boolean;
    /**
     * Bonds with the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    bond(): Promise<boolean>;
    /**
     * Unbonds from the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    unbond(): Promise<boolean>;
    /**
     * Retrieves the latest measurement
     * @returns { Promise<Observation> } A promise resolving to the latest measurement
     */
    getDeviceAndMeasurement(): Promise<{
        device: Device;
        measurement: Observation;
    }>;
    /**
     * method to be called upon receiving an observation
     * @param measurementId id of the measurement
     * @param observation the observation
     * @param device the device
     * @param meta metadata
     */
    private measurementReceived;
}
