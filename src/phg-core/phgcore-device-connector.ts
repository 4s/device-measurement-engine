import { DeviceConnector } from './device-connector'
import { Device, Observation } from 'fhir'
import { MeasurementReceived } from 'phg-messages/types/FHIRMeasurement_pb'
import { InterfaceEndpoint, MetaData, MessageType, Baseplate } from 'phg-js-baseplate'

export class PHGCoreDeviceConnector implements DeviceConnector {
  private consumer: InterfaceEndpoint
  private device: Device
  private observation: Observation

  public constructor(baseplate: Baseplate) {
    const moduleBase = new baseplate.ModuleBase("phgcore-device-connector")
    try {
      this.consumer = moduleBase.implementsConsumerInterface("FHIRMeasurement");
    } catch (e) {
      throw new Error('error while calling implementsConsumerInterface: ' + e)
    }
    try {
      this.consumer.addEventHandler("MeasurementReceived",
        (input: MeasurementReceived, meta: MetaData) => {
          this.measurementReceived(input, meta)
        }
      )
    } catch (e) {
      throw new Error('error while adding eventhandler: ' + e)
    }
    moduleBase.start();
  }

  /**
   * Returns whether or not the device is bonded
   * @returns boolean
   */
  public isBonded(): boolean {
    throw new Error('function not implemented')
  }

  /**
   * Bonds with the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public bond(): Promise<boolean> {
    throw new Error('function not implemented')
  }

  /**
   * Unbonds from the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public unbond(): Promise<boolean> {
    throw new Error('function not implemented')
  }

  /**
   * Retrieves the latest measurement
   * @returns { Promise<Observation> } A promise resolving to the latest measurement
   */
  public getDeviceAndMeasurement(): Promise<{ device: Device, measurement: Observation }> {
    return new Promise<{ device: Device, measurement: Observation }>((resolve, reject) => {
      const wait = () => {
        if (this.observation && this.device) {
          return resolve({ device: this.device, measurement: this.observation });
        }
        setTimeout(wait.bind(this, resolve, reject), 2);
      }
      wait();
    });
  }

  /**
   * method to be called upon receiving an observation
   * @param measurementId id of the measurement
   * @param observation the observation
   * @param device the device
   * @param meta metadata
   */
  private measurementReceived(measurement: MeasurementReceived, meta: MetaData) {
    const observation = measurement.getFhirObservation()
    const device = measurement.getFhirDevice()

    /* tslint:disable */
    console.log("measurementReceived(\"" + observation + "\", \"" + device + "\", { " + "messageType: MessageType." +
      MessageType[meta.messageType] + ", sender: \"" + meta.sender + "\", signal: \"" + meta.signal + "\"})")
    /* tslint:enable */

    if (typeof observation !== "string" || typeof device !== "string") {
      throw new Error('Malformed json')
    }
    this.observation = JSON.parse(observation)
    this.device = JSON.parse(device)
  }
}