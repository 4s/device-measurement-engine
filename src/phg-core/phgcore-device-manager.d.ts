import { DeviceManager } from './device-manager';
import { DeviceConnector } from './device-connector';
import { code } from 'fhir';
import { Baseplate } from 'phg-js-baseplate';
export declare class PHGCoreDeviceManager implements DeviceManager {
    private baseplate;
    constructor(baseplate: Baseplate);
    /**
     * Returns all currently available devices of the types provided as codes (Not finished)
     * @param { codes } code[] - The MDC-codes of the type of devices
     * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
     */
    getAvailableDevices(codes: code[]): Promise<DeviceConnector[]>;
    removeDuplicatesPredicate(): (item: string, index: number, array: string[]) => boolean;
    /**
     * Searches for devices of of the types provided as codes (Not finished)
     * @param { codes } code[] - The MDC-codes of the type of devices
     * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
     */
    searchForDevices(codes: code[]): Promise<DeviceConnector[]>;
}
