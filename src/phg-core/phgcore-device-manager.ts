import { DeviceManager } from './device-manager'
import { DeviceConnector } from './device-connector'
import { code } from 'fhir'
import { PHGCoreDeviceConnector } from './phgcore-device-connector';
import { MonicaDeviceConnector } from './monica-device-connector';
import { TestDeviceConnector } from './test-device-connector';
import { Baseplate } from 'phg-js-baseplate';

export class PHGCoreDeviceManager implements DeviceManager {
  private baseplate: Baseplate

  constructor(baseplate: Baseplate) {
    this.baseplate = baseplate
  }

  /**
   * Returns all currently available devices of the types provided as codes (Not finished)
   * @param { codes } code[] - The MDC-codes of the type of devices
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
   */
  public getAvailableDevices(codes: code[]): Promise<DeviceConnector[]> {
    const devices: DeviceConnector[] = []
    // loop over array where duplicates have been removed
    codes.filter(this.removeDuplicatesPredicate()).forEach(code => {
      switch (code.toLowerCase()) {
        case 'mdc8450059': case '8450059': case '585728':
          devices.push(new MonicaDeviceConnector(this.baseplate))
          break;
        case 'test':
          devices.push(new TestDeviceConnector())
          break;
        default:
          devices.push(new PHGCoreDeviceConnector(this.baseplate))
          break;
      }
    })
    return new Promise<DeviceConnector[]>((resolve, _) => {
      resolve(devices)
    })
  }

  removeDuplicatesPredicate() {
    return (item: string, index: number, array: string[]) => array.indexOf(item) == index;
  }

  /**
   * Searches for devices of of the types provided as codes (Not finished)
   * @param { codes } code[] - The MDC-codes of the type of devices
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
   */
  public searchForDevices(codes: code[]): Promise<DeviceConnector[]> {
    const devices: DeviceConnector[] = []
    // loop over array where duplicates have been removed
    codes.filter(this.removeDuplicatesPredicate()).forEach(code => {
      switch (code.toLowerCase()) {
        case 'mdc8450059': case '8450059': case '585728':
          devices.push(new MonicaDeviceConnector(this.baseplate))
          break;
        case 'test':
          devices.push(new TestDeviceConnector())
          break;
        default:
          devices.push(new PHGCoreDeviceConnector(this.baseplate))
          break;
      }
    })
    return new Promise<DeviceConnector[]>((resolve, _) => {
      resolve(devices)
    })
  }
}
