import { DeviceConnector } from './device-connector';
import { Device, Observation } from 'fhir';
/** A class representing a DeviceConnector using mockdata */
export declare class TestDeviceConnector implements DeviceConnector {
    /**
     * Whether or not the device is bonded
     * @private
     * @type { DeviceManager }
     * @ignore
     */
    private bonded;
    /**
     * Creates a new TestDeviceConnector
     */
    constructor();
    /**
     * Returns whether or not the device is bonded
     * @returns boolean
     */
    isBonded(): boolean;
    /**
     * <MOCK> Bonds with the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    bond(): Promise<boolean>;
    /**
     * <MOCK> Unbonds from the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    unbond(): Promise<boolean>;
    /**
     * <MOCK> Starts a measurement
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    startMeasurement(): Promise<boolean>;
    /**
     * <MOCK> Stops the running measurement
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    stopMeasurement(): Promise<boolean>;
    /**
     * <MOCK> Retrieves the latest measurement
     * @returns { Promise<Observation> } A promise resolving to the latest measurement
     */
    getDeviceAndMeasurement(): Promise<{
        device: Device;
        measurement: Observation;
    }>;
}
