import { Device, Observation } from 'fhir';
import { DeviceConnector } from './device-connector';
/** A class representing a DeviceConnector using PHG Core */
export declare class TestMonicaDeviceConnector implements DeviceConnector {
    /**
     * statusMessage for Monica
     */
    private statusMessage;
    /**
     * Creates a new TestDeviceConnector
     */
    constructor();
    /**
     * Returns whether or not the device is bonded
     * @returns boolean
     */
    isBonded(): boolean;
    /**
     * Bonds with the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    bond(): Promise<boolean>;
    /**
     * Unbonds from the device
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    unbond(): Promise<boolean>;
    /**
     * Starts a measurement
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    startMeasurement(): Promise<boolean>;
    /**
     * Stops the running measurement
     * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
     */
    stopMeasurement(): Promise<boolean>;
    /**
     * Retrieves the latest measurement
     * @returns { Promise<Observation> } A promise resolving to the latest measurement
     */
    getDeviceAndMeasurement(): Promise<{
        device: Device;
        measurement: Observation;
    }>;
    /**
     * Returns current  status
     */
    readonly status: string;
}
