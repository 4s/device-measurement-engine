import { Device, Observation } from 'fhir'
import { ObservationBuilder } from '../observation-builder'
import { DeviceBuilder } from '../device-builder'
import { CodeableConceptBuilder } from '../codeable-concept-builder'
import { IdentifierBuilder } from '../identifier-builder'
import { DeviceConnector } from './device-connector'

/** A class representing a DeviceConnector using PHG Core */
export class TestMonicaDeviceConnector implements DeviceConnector {

  /**
   * statusMessage for Monica
   */
  private statusMessage: string

  /**
   * Creates a new TestDeviceConnector
   */
  public constructor() {
    this.statusMessage = 'No statusmessage received'
  }

  /**
   * Returns whether or not the device is bonded
   * @returns boolean
   */
  public isBonded(): boolean {
    throw new Error('function not implemented')
  }
  /**
   * Bonds with the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public bond(): Promise<boolean> {
    throw new Error('function not implemented')
  }
  /**
   * Unbonds from the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public unbond(): Promise<boolean> {
    throw new Error('function not implemented')
  }
  /**
   * Starts a measurement
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public startMeasurement(): Promise<boolean> {
    return new Promise<boolean>((resolve, _) => {
      setTimeout(() => {
        this.statusMessage = 'Recording started'
        resolve(true)
      }, 1000)
    })
  }
  /**
   * Stops the running measurement
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public stopMeasurement(): Promise<boolean> {
    return new Promise<boolean>((resolve, _) => {
      setTimeout(() => {
        this.statusMessage = 'Recording stopped'
        resolve(true)
      }, 1000)
    })
  }
  /**
   * Retrieves the latest measurement
   * @returns { Promise<Observation> } A promise resolving to the latest measurement
   */
  public getDeviceAndMeasurement(): Promise<{ device: Device, measurement: Observation }> {
    return new Promise<{ device: Device, measurement: Observation }>((resolve, _) => {
      setTimeout(() => {
        this.statusMessage = 'Observation returned'
        const measurement: Observation = new ObservationBuilder()
          .setValueInteger(37)
          .setDevice(
            null,
            new IdentifierBuilder()
              .setSystem('urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680')
              .setValue('7E-ED-AB-EE-34-AD-BE-EF')
              .build()
          )
          .build()

        const device: Device = new DeviceBuilder()
          .addIdentifier(
            new IdentifierBuilder()
              .setUse('official')
              .setSystem('urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680')
              .setValue('7E-ED-AB-EE-34-AD-BE-EF')
              .build()
          )
          .setType(
            new CodeableConceptBuilder()
              .addCoding(
                'urn:iso:std:iso:11073:10101',
                '531981',
                'MDC_MOC_VMS_MDS_AHD'
              )
              .setText('Personal Health Gateway')
              .build()
          )
          .setManufacturer('Alexandra Instituttet A/S')
          .setModelNumber('4S PHG Proof-of-Concept')
          .build()
        resolve({ device, measurement })
      }, 1000)
    })
  }

  /**
   * Returns current  status
   */
  public get status(): string {
    return this.statusMessage
  }
}