
import { Device, Observation } from 'fhir'
import { ObservationBuilder } from '../observation-builder'
import { DeviceBuilder } from '../device-builder'
import { CodeableConceptBuilder } from '../codeable-concept-builder'
import { IdentifierBuilder } from '../identifier-builder'
import { DeviceConnector } from './device-connector'

/** A class representing a DeviceConnector using mockdata */
export class TestPHGCoreDeviceConnector implements DeviceConnector {
  /**
   * Whether or not the device is bonded
   * @private
   * @type { DeviceManager }
   * @ignore
   */
  private bonded: boolean
  /**
   * Creates a new TestDeviceConnector
   */
  public constructor() {
    this.bonded = false
  }
  /**
   * Returns whether or not the device is bonded
   * @returns boolean
   */
  public isBonded(): boolean {
    return this.bonded
  }
  /**
   * <MOCK> Bonds with the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public bond(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this.bonded) {
        reject(new Error('Cannot bond with device. Device already bonded'))
      }
      resolve(this.bonded = true)
    })
  }
  /**
   * <MOCK> Unbonds from the device
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public unbond(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.bonded) {
        reject(new Error('Cannot unbond with device. Device not bonded'))
      }
      resolve(this.bonded = false)
    })
  }
  /**
   * <MOCK> Starts a measurement
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public startMeasurement(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      resolve(true)
    })
  }

  /**
   * <MOCK> Stops the running measurement
   * @returns { Promise<boolean> } A promise resolving to a boolean indicating whether the operation was successful
   */
  public stopMeasurement(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      resolve(true)
    })
  }
  /**
   * <MOCK> Retrieves the latest measurement
   * @returns { Promise<Observation> } A promise resolving to the latest measurement
   */
  public getDeviceAndMeasurement(): Promise<{ device: Device, measurement: Observation }> {
    return new Promise<{ device: Device, measurement: Observation }>(resolve => {
      const measurement: Observation = new ObservationBuilder()
        .setValueInteger(37)
        .setDevice(
          null,
          new IdentifierBuilder()
            .setSystem('urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680')
            .setValue('7E-ED-AB-EE-34-AD-BE-EF')
            .build()
        )
        .build()

      const device: Device = new DeviceBuilder()
        .addIdentifier(
          new IdentifierBuilder()
            .setUse('official')
            .setSystem('urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680')
            .setValue('7E-ED-AB-EE-34-AD-BE-EF')
            .build()
        )
        .setType(
          new CodeableConceptBuilder()
            .addCoding(
              'urn:iso:std:iso:11073:10101',
              '531981',
              'MDC_MOC_VMS_MDS_AHD'
            )
            .setText('Personal Health Gateway')
            .build()
        )
        .setManufacturer('Alexandra Instituttet A/S')
        .setModelNumber('4S PHG Proof-of-Concept')
        .build()
      resolve({ device, measurement })
    })
  }
}
