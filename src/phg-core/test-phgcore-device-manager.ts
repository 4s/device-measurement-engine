import { DeviceManager } from './device-manager'
import { DeviceConnector } from './device-connector'
import { code } from 'fhir'
import { TestPHGCoreDeviceConnector } from './test-phgcore-device-connector';
import { TestMonicaDeviceConnector } from './test-monica-device-connector';

/** A class representing a DeviceManager using mockdata */
export class TestPHGCoreDeviceManager implements DeviceManager {
  /**
   * Returns all currently available devices of the types provided as codes (Not finished)
   * @param { codes } code[] - The MDC-codes of the type of devices
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
   */
  public getAvailableDevices(codes: code[]): Promise<DeviceConnector[]> {

    const devices: DeviceConnector[] = []
    // loop over array where duplicates have been removed
    const uniqueArray = Array.from(new Set(codes));
    uniqueArray.forEach(code => {
      switch (code.toLowerCase()) {
        case 'mdc8450059': case '8450059':
          devices.push(new TestMonicaDeviceConnector())
          break;
        default:
          devices.push(new TestPHGCoreDeviceConnector())
          break;
      }
    })

    return new Promise<any[]>((resolve, _) => {
      resolve(devices)
    })
  }

  /**
   * Searches for devices of of the types provided as codes (Not finished)
   * @param { codes } code[] - The MDC-codes of the type of devices
   * @returns { Promise<DeviceConnector[]> } A promise to be resolved into an array of objects representing devices
   */
  public searchForDevices(codes: code[]): Promise<DeviceConnector[]> {
    const devices: DeviceConnector[] = []
    // loop over array where duplicates have been removed
    const uniqueArray = Array.from(new Set(codes));
    uniqueArray.forEach(code => {
      switch (code.toLowerCase()) {
        case 'mdc8450059': case '8450059':
          devices.push(new TestMonicaDeviceConnector())
          break;
        default:
          devices.push(new TestPHGCoreDeviceConnector())
          break;
      }
    })
    return new Promise<any[]>((resolve, _) => {
      resolve(devices)
    })
  }
}
