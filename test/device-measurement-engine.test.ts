import { ObservationBuilder } from '../src/observation-builder'
import { Observation } from 'fhir'
import testObservation from './resources/test_observation.json'
import DeviceMeasurementEngine, {
  PHGCoreDeviceManager,
} from '../src/device-measurement-engine'
import { Baseplate } from 'phg-js-baseplate'

describe('DeviceMeasurementEngine', () => {
  let observation: Observation
  let deviceMeasurementEngine: DeviceMeasurementEngine
  let observationBuilder: ObservationBuilder
  let baseplate: Baseplate

  beforeEach(() => {
    observation = { ...testObservation } as Observation

    deviceMeasurementEngine = new DeviceMeasurementEngine(
      new PHGCoreDeviceManager(baseplate)
    )
    observationBuilder = deviceMeasurementEngine.getObservationBuilder()
    observationBuilder.setObservation(observation)
  })

  afterEach(() => {
    observation = null
    observationBuilder.setObservation(null)
    observationBuilder = null
    deviceMeasurementEngine = null
  })
})
