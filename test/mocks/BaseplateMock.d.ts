import { Baseplate, ApplicationState, CoreError, ModuleBase } from "phg-js-baseplate";
export declare class BaseplateMock implements Baseplate {
    isAvailable(): boolean;
    ModuleBase: typeof ModuleBase;
    CoreError: typeof CoreError;
    ApplicationState: typeof ApplicationState;
}
