import { Baseplate, ApplicationState, CoreError } from "phg-js-baseplate";
import { ModuleBaseMock } from "./ModuleBaseMock.test";
export declare class BaseplateMock implements Baseplate {
    isAvailable(): boolean;
    ModuleBase: typeof ModuleBaseMock;
    CoreError: typeof CoreError;
    ApplicationState: typeof ApplicationState;
}
