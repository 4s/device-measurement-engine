import { Baseplate, ApplicationState, CoreError, ModuleBase } from "phg-js-baseplate";
import { ModuleBaseMock } from "./ModuleBaseMock.test";


export class BaseplateMock implements Baseplate {

    isAvailable() {
        return true
    }

    ModuleBase = ModuleBaseMock
    CoreError: typeof CoreError
    ApplicationState: typeof ApplicationState

}