export declare class CurrentStatusMock {
    currentStatus: string;
    setCurrentStatus(status: string): void;
    getCurrentStatus(): string;
}
