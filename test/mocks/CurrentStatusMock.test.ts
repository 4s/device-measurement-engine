
export class CurrentStatusMock {

    currentStatus: string

    setCurrentStatus(status: string) {
        this.currentStatus = status
    }

    getCurrentStatus(): string {
        return this.currentStatus
    }


}