import { CoreError, InterfaceEndpoint, MetaData } from "phg-js-baseplate";
export declare class InterfaceEndpointMock implements InterfaceEndpoint {
    private eventNames;
    private moduleTag;
    constructor(moduleTag: string);
    addFunctionHandler(functionName: string, implementation: (args: any, outputClass: any, callback: (argsOrError: any, more: boolean) => void, messagesRoot: any, meta: MetaData) => void): void;
    addEventHandler(eventName: string, implementation: (args: any, meta: MetaData) => void): void;
    callFunction(functionName: string, argumentProvider: (args: any) => void, destination: string, successCallback: (args: any, more: boolean) => void, errorCallback: (error: CoreError, more: boolean) => void, signal?: string): void;
    postEvent(eventName: string, argumentProvider: (args: any) => void, destination?: string, signal?: string): void;
    createError(errorCode: number, message: string): CoreError;
    getEventNames(): string[];
}
