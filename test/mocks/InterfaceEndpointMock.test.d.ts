import { CoreError, InterfaceEndpoint, MetaData } from "phg-js-baseplate";
export declare class InterfaceEndpointMock implements InterfaceEndpoint {
    private eventNames;
    private moduleTag;
    private postedEvents;
    private implementation;
    constructor(moduleTag: string);
    addFunctionHandler(functionName: string, implementation: (args: any, outputClass: any, callback: (argsOrError: any, more: boolean) => void, messagesRoot: any, meta: MetaData) => void): void;
    addEventHandler(eventName: string, implementation: (input: any, meta: any) => void): void;
    callFunction(functionName: string, argumentProvider: (args: any) => void, destination: string, successCallback: (args: any, more: boolean) => void, errorCallback: (error: CoreError, more: boolean) => void, signal?: string): void;
    postEvent(eventName: string, argumentProvider: (args: any) => void, destination?: string, signal?: string): void;
    createError(errorCode: number, message: string): CoreError;
    getEventNames(): string[];
    getModuleTag(): string;
    getPostedEvents(): string[];
    getImplmentation(): (input: any, meta: MetaData) => void;
}
