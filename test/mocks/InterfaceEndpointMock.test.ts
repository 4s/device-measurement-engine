import { CoreError, InterfaceEndpoint, MetaData } from "phg-js-baseplate";
import { MeasurementReceived } from 'phg-messages/types/FHIRMeasurement_pb'


export class InterfaceEndpointMock implements InterfaceEndpoint {

    private eventNames: string[]
    private moduleTag: string
    private postedEvents: string[]
    private implementation: (input: MeasurementReceived, meta: MetaData) => void

    public constructor(moduleTag: string) {
        this.eventNames = []
        this.postedEvents = []
        this.moduleTag = moduleTag
    }

    addFunctionHandler(functionName: string,
        implementation: (args: any,
            outputClass: any,
            callback: (argsOrError: any,
                more: boolean) => void,
            messagesRoot: any,
            meta: MetaData
        ) => void): void {

    }

    addEventHandler(eventName: string,
        implementation: (input: any, meta: any) => void
    ): void {
        this.eventNames.push(eventName)
        this.implementation = implementation
    }

    callFunction(functionName: string,
        argumentProvider: (args: any) => void,
        destination: string,
        successCallback: (args: any, more: boolean) => void,
        errorCallback: (error: CoreError, more: boolean) => void,
        signal?: string
    ): void {

    }

    postEvent(eventName: string,
        argumentProvider: (args: any) => void,
        destination?: string,
        signal?: string
    ): void {
        this.postedEvents.push(eventName)
    }

    createError(errorCode: number, message: string): CoreError {
        return new CoreError("moduleTag", errorCode + ": " + message);
    }

    public getEventNames(): string[] {
        return this.eventNames;
    }

    public getModuleTag(): string {
        return this.moduleTag
    }

    public getPostedEvents(): string[] {
        return this.postedEvents
    }

    public getImplmentation(): (input: any, meta: MetaData) => void {
        return this.implementation
    }
}