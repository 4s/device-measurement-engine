export declare class MeasurementReceivedMock {
    deviceType: string;
    observation: string;
    device: string;
    getDeviceType(): string;
    setDeviceType(value: string): void;
    getFhirObservation(): string;
    setFhirObservation(value: string): void;
    getFhirDevice(): string;
    setFhirDevice(value: string): void;
    serializeBinary(): Uint8Array;
    getExtension<T>(): T;
    getJsPbMessageId(): string;
    serializeBinaryExtensions(): void;
    readBinaryExtension(): void;
    toArray(): any[];
    setExtension(): void;
    cloneMessage(): this;
    clone(): this;
    toObject(): {
        deviceType: string;
        fhirObservation: string;
        fhirDevice: string;
    };
}
