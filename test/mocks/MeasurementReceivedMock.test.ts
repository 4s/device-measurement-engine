

export class MeasurementReceivedMock {

    deviceType: string
    observation: string
    device: string

    getDeviceType() {
        return this.deviceType
    }

    setDeviceType(value: string) {
        this.deviceType = value
    }

    getFhirObservation() {
        return this.observation
    }

    setFhirObservation(value: string) {
        this.observation = value
    }


    getFhirDevice() {
        return this.device
    }

    setFhirDevice(value: string) {
        this.device = value
    };

    serializeBinary() {
        return new Uint8Array(32)
    }

    getExtension<T>(): T {
        var something: T
        return something
    }

    getJsPbMessageId() {
        return ""
    }
    serializeBinaryExtensions() { }
    readBinaryExtension() { }
    toArray(): any[] {
        return []
    }

    setExtension() { }
    cloneMessage() { return this }
    clone() { return this }

    toObject() {
        return {
            deviceType: "string",
            fhirObservation: "string",
            fhirDevice: "string",
        }
    }
}