/**
 * Represents the destination type of a message.
 */
declare enum MessageType {
    UNICAST = 85,
    MULTICAST = 77,
    BROADCAST = 66
}