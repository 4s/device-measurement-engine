export declare class MetaDataMock {
    messageType: MessageType;
    sender: string;
    signal: string;
}
