import { MetaData } from "phg-js-baseplate"


export class MetaDataMock {

    messageType: MessageType

    sender: string = ""

    signal: string = ""
}