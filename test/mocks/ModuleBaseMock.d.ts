import { ModuleBase, InterfaceEndpoint } from "phg-js-baseplate";
export declare class ModuleBaseMock extends ModuleBase {
    private interfaceEndpoint;
    private messages;
    constructor(moduleTag: string);
    implementsConsumerInterface(interfaceName: string): InterfaceEndpoint;
    start(): void;
    postEvent(message: string): void;
}
