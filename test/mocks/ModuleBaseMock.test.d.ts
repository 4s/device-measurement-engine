import { InterfaceEndpoint, ApplicationState } from "phg-js-baseplate";
export declare class ModuleBaseMock {
    private messages;
    constructor(moduleTag: string);
    implementsConsumerInterface(interfaceName: string): InterfaceEndpoint;
    implementsProducerInterface(interfaceName: string): InterfaceEndpoint;
    start(): void;
    postEvent(message: string): void;
    onApplicationStateChange(): void;
    getId(): string;
    getModuleTag(): string;
    getApplicationState(): ApplicationState;
    createObjId(): string;
}
