import { ModuleBase, InterfaceEndpoint, ApplicationState } from "phg-js-baseplate";
import { InterfaceEndpointMock } from "./InterfaceEndpointMock.test";

export class ModuleBaseMock {

    private messages: string[]

    public constructor(moduleTag: string) {
        this.messages = []
    }

    implementsConsumerInterface(interfaceName: string): InterfaceEndpoint {
        return new InterfaceEndpointMock(interfaceName)
    }

    implementsProducerInterface(interfaceName: string): InterfaceEndpoint {
        return new InterfaceEndpointMock(interfaceName)
    }

    public start() {
    }

    public postEvent(message: string) {
        console.log(message)
    }

    onApplicationStateChange() {

    }

    getId(): string {
        return "54"
    }

    getModuleTag() {
        return "string"
    }

    getApplicationState() {
        return ApplicationState.READY
    }

    createObjId() {
        return "12341"
    }
}