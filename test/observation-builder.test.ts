import { expect } from 'chai'
import { ObservationBuilder } from '../src/observation-builder'
import { Observation, dateTime, ObservationDefinition, ObservationDefinitionQuantitativeDetails } from 'fhir'
import testObservation from './resources/test_observation.json'
import { IdentifierBuilder } from '../src/identifier-builder'

describe('ObservationBuilder', () => {
  let observation: Observation
  let observationBuilder: ObservationBuilder

  beforeEach(() => {
    observation = { ...testObservation } as Observation
    observationBuilder = new ObservationBuilder()
    observationBuilder.setObservation(observation)
  })

  it('can add identifier when none of same system exist', () => {
    var identifierBuilder = new IdentifierBuilder()
    identifierBuilder.setSystem("theSystem")
    identifierBuilder.setValue("theValue")
    var identifier = identifierBuilder.build()

    observationBuilder.addIdentifier(identifier)

    expect(observationBuilder.getIdentifier("theSystem").value).to.equal("theValue")
  })

  it('can not add identifier when one of same system exist', () => {
    var identifierBuilder = new IdentifierBuilder()
    identifierBuilder.setSystem("theSystem")
    identifierBuilder.setValue("theValue")
    var identifier = identifierBuilder.build()

    observationBuilder.addIdentifier(identifier)
    expect(() => observationBuilder.addIdentifier(identifier)).to.throw()
  })

  it('can remove identifier when one exist', () => {
    var identifierBuilder = new IdentifierBuilder()
    identifierBuilder.setSystem("theSystem")
    identifierBuilder.setValue("theValue")
    var identifier = identifierBuilder.build()

    observationBuilder.addIdentifier(identifier)

    expect(observationBuilder.removeIdentifier("theSystem")).to.equal(true)
  })

  it('can not remove identifier when none exist', () => {
    var identifierBuilder = new IdentifierBuilder()
    identifierBuilder.setSystem("theSystem")
    identifierBuilder.setValue("theValue")
    var identifier = identifierBuilder.build()

    observationBuilder.addIdentifier(identifier)

    expect(observationBuilder.removeIdentifier("somethingElse")).to.equal(false)
  })


  it('can set basedOn', () => {
    expect(observationBuilder.getBasedOn()).to.equal(undefined)
    observationBuilder.setBasedOn()
    expect(observationBuilder.getBasedOn()).to.not.equal(undefined)
  })

  it('can remove basedOn', () => {
    expect(observationBuilder.getBasedOn()).to.equal(undefined)
    observationBuilder.setBasedOn()
    expect(observationBuilder.getBasedOn()).to.not.equal(undefined)
    expect(observationBuilder.removeBasedOn()).to.equal(true)
    expect(observationBuilder.getBasedOn()).to.equal(undefined)
  })


  it('can set status', () => {
    expect(observationBuilder.setStatus("registered").getStatus()).to.equal("registered")
  })

  it('can not set invalid status', () => {
    expect(() => observationBuilder.setStatus("invalid")).to.throw()
  })

  it('can not set effective dateTime if it is already set', () => {
    expect(() => observationBuilder.setEffectiveDateTime(null)).to.throw()
  })

  it('can not set effective period if it is already set', () => {
    expect(() => observationBuilder.setEffectivePeriod()).to.throw()
  })

  it('can not set effective Instant if it is already set', () => {
    expect(() => observationBuilder.setEffectiveInstant(null)).to.throw()
  })

  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueBoolean(null)).to.throw()
  })

  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueDateTime(null)).to.throw()
  })

  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueInteger(null)).to.throw()
  })


  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueString(null)).to.throw()
  })


  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueTime(null)).to.throw()
  })


  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValuePeriod(null)).to.throw()
  })


  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueQuantity(null)).to.throw()
  })


  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueCodeableConcept(null)).to.throw()
  })


  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueRange(null)).to.throw()
  })


  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueRatio(null)).to.throw()
  })

  it('can not set value boolean if it is already set', () => {
    expect(() => observationBuilder.setValueSampledData(null, null, null, null, null, null, null)).to.throw()
  })

  it('can set device identifier if reference is undefined', () => {
    observationBuilder.setDevice(undefined, { "system": "42", "value": "43" }, null)

    expect(observationBuilder.getDevice().identifier).to.deep.equal({ "system": "42", "value": "43" })
  })

  it('can set device reference when it is not undefined, and identifier is not set', () => {
    observationBuilder.setDevice(null, { "system": "42", "value": "43" }, null)

    expect(observationBuilder.getDevice().identifier).to.equal(undefined)
  })


  it('can set hasMember identifier if reference is undefined', () => {
    observationBuilder.setHasMember(undefined, { "system": "42", "value": "43" }, null)

    expect(observationBuilder.getHasMember()[0].identifier).to.deep.equal({ "system": "42", "value": "43" })
  })

  it('can set hasMember reference when it is not undefined, and identifier is not set', () => {
    observationBuilder.setHasMember(null, { "system": "42", "value": "43" }, null)

    expect(observationBuilder.getHasMember()[0].identifier).to.equal(undefined)
  })

  it('can add hasMember. If no hasMember is set, a new array is made', () => {
    observationBuilder.removeAllHasMembers()

    expect(observationBuilder.getHasMember()).to.equal(undefined)

    observationBuilder.addHasMember(null, { "system": "42", "value": "43" }, null)

    expect(observationBuilder.getHasMember()).to.not.equal(undefined)
  })

  it('can add hasMember. If reference is undefined, identifier is set', () => {
    observationBuilder.addHasMember(undefined, { "system": "42", "value": "43" }, null)

    expect(observationBuilder.getHasMember()[0].identifier).to.deep.equal({ "system": "42", "value": "43" })
  })

  it('can add hasMember. If reference is not undefined, identifier is not set', () => {
    observationBuilder.addHasMember(null, { "system": "42", "value": "43" }, null)

    expect(observationBuilder.getHasMember()[0].identifier).to.equal(undefined)
  })

  it('can remove hasMember, based on reference', () => {
    observationBuilder.addHasMember("reference", { "system": "42", "value": "43" }, null)

    expect(observationBuilder.removeHasMember("reference", null)).to.equal(true)
  })

  it('can remove hasMember, based on identifier when reference is undefined', () => {
    let identifier = { "system": "42", "value": "43" }
    observationBuilder.addHasMember(undefined, identifier, null)

    expect(observationBuilder.removeHasMember(undefined, identifier)).to.equal(true)
  })


  it('cannot remove hasMember, if neither argument is defined', () => {
    expect(observationBuilder.removeHasMember(undefined, undefined)).to.equal(false)
  })

  it('cannot add component if it has one with the same code', () => {
    let code = {
      "code": {
        "coding": [
          {
            "system": "system",
            "code": "code"
          }
        ]
      }
    }
    observationBuilder.addComponent(code)
    expect(() => observationBuilder.addComponent(code)).to.throw()
  })

  it('can return something meaningful if when calling getComponent, even if it is not set', () => {
    expect(observationBuilder.getComponent(null, null)).to.equal(null)
  })

  it('can set observation from Observationdefinition with code', () => {
    let observationDefinition = {
      code: {
        coding: [{
          system: "system",
          code: "code"
        }]
      },
    } as ObservationDefinition
    observationBuilder.observationFromDefinition(observationDefinition)

    let obs = observationBuilder.build()

    expect(obs.code).to.deep.equal({
      "coding": [{
        "system": "system",
        "code": "code"
      }]
    })
  })

  it('can set observation from Observationdefinition with cateory', () => {
    let observationDefinition = {
      code: {
        system: "",
        code: ""
      },
      category: [{
        coding: [{
          system: "system",
          code: "code"
        }]
      }],
    } as ObservationDefinition
    observationBuilder.observationFromDefinition(observationDefinition)

    let obs = observationBuilder.build()
    expect(obs.category).to.deep.equal([{
      coding: [{
        system: "system",
        code: "code"
      }]
    }])
  })


  it('can set observation from Observationdefinition with method', () => {
    let observationDefinition: ObservationDefinition = {
      "code": {
        "system": "",
        "code": ""
      },
      "method": {
        "coding": [
          {
            "system": "system",
            "code": "code"
          }
        ]
      },
    }
    observationBuilder.observationFromDefinition(observationDefinition)

    let obs = observationBuilder.build()
    expect(obs.method).to.deep.equal({
      "coding": [{
        "system": "system",
        "code": "code"
      }]
    })
  })

  it('can set observation from Observationdefinition with referenceRange', () => {
    let observationDefinition: ObservationDefinition = {
      "code": {
        "system": "",
        "code": ""
      },
      "qualifiedInterval": [{
        age: {
          low: {
            value: 42
          },
          high: {
            value: 420
          }
        },

      }]

    }
    observationBuilder.observationFromDefinition(observationDefinition)

    let obs = observationBuilder.build()
    expect(obs.referenceRange[0].age).to.deep.equal({
      low: {
        value: 42
      },
      high: {
        value: 420
      }
    })
    expect(obs.referenceRange[0].appliesTo).to.equal(undefined)
    expect(obs.referenceRange[0].type).to.equal(undefined)
    expect(obs.referenceRange.length).to.equal(1)
  })

  it('can set Observation from ObsevationDefinition with url and version', () => {
    let observationDefinition: ObservationDefinition = {
      "extension": [
        {
          "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/186744833/ObservationDefinition+url+extension",
          "valueUri": "canonical://uuid/ObservationDefinition/6186479b-f7cc-42f8-aa7f-2ecfc048f426"
        },
        {
          "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/197066753/ObservationDefinition+version+extension",
          "valueString": "1.0.0"
        }
      ],
      "code": {
        "system": "system",
        "code": "code"
      },
    }
    observationBuilder.observationFromDefinition(observationDefinition)

    let obs = observationBuilder.build()

    expect(obs.extension).to.deep.equal([
      {
        "url": "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110723073/Observation+definition+extension",
        "valueCanonical": "canonical://uuid/ObservationDefinition/6186479b-f7cc-42f8-aa7f-2ecfc048f426|1.0.0"
      }
    ])

  })

  it('should throw error when setting value twice', () => {
    expect(() => observationBuilder.setValueBoolean(true)).to.throw()
  })

  it('should allow setting valueBoolean', () => {
    if (observationBuilder.removeValueQuantity()) {
      observationBuilder.setValueBoolean(true)
    }
    observation = observationBuilder.build()

    expect(observation.valueBoolean).to.equal(true)
  })

  it('should allow setting valueCodeableConcept', () => {
    if (observationBuilder.removeValueQuantity()) {
      observationBuilder.setValueCodeableConcept(
        'test-text',
        'urn:oid:1.2.208.176.2.1',
        null,
        'NPU02187',
        'Glukose;B',
        false
      )
    }
    observation = observationBuilder.build()

    expect(observation.valueCodeableConcept).to.eql({
      coding:
        [{
          system: 'urn:oid:1.2.208.176.2.1',
          version: null,
          code: 'NPU02187',
          display: 'Glukose;B',
          userSelected: false
        }],
      text: 'test-text'
    })
  })

  it('should allow setting valueDateTime', () => {
    if (observationBuilder.removeValueQuantity()) {
      observationBuilder.setValueDateTime('25-05-2016')
    }
    observation = observationBuilder.build()

    expect(observation.valueDateTime).to.equal('25-05-2016')
  })

  it('should allow setting valueInteger', () => {
    if (observationBuilder.removeValueQuantity()) {
      observationBuilder.setValueInteger(15)
    }
    observation = observationBuilder.build()
    expect(observation.valueInteger).to.equal(15)
  })

  it('should allow setting valuePeriod', () => {
    if (observationBuilder.removeValueQuantity()) {
      observationBuilder.setValuePeriod('25-05-2016', '26-05-2016')
    }
    observation = observationBuilder.build()

    expect(observation.valuePeriod).to.eql({ start: '25-05-2016', end: '26-05-2016' })
  })

  it('should allow setting valueQuantity', () => {
    if (observationBuilder.removeValueQuantity()) {
      observationBuilder.setValueQuantity(
        5.3,
        null,
        'mmol/L',
        'urn:oid:1.2.208.176.2.1',
        'NPU02187'
      )
    }
    observation = observationBuilder.build()

    expect(observation.valueQuantity).to.eql({
      value: 5.3,
      comparator: null,
      unit: 'mmol/L',
      system: 'urn:oid:1.2.208.176.2.1',
      code: 'NPU02187'
    })
  })

  it('should allow setting valueRange', () => {
    if (observationBuilder.removeValueQuantity()) {
      observationBuilder.setValueRange(
        5,
        10,
        'mmol/L',
        'urn:oid:1.2.208.176.2.1'
      )
    }
    observation = observationBuilder.build()

    expect(observation.valueRange).to.eql({
      low:
      {
        value: 5,
        unit: 'mmol/L',
        system: 'urn:oid:1.2.208.176.2.1',
        code: undefined
      },
      high:
      {
        value: 10,
        unit: 'mmol/L',
        system: 'urn:oid:1.2.208.176.2.1',
        code: undefined
      }
    })
  })

  it('should allow setting valuePeriod', () => {
    if (observationBuilder.removeValueQuantity()) {
      observationBuilder.setValueRatio(7, 13, 'mmol/L')
    }
    observation = observationBuilder.build()

    expect(observation.valueRatio).to.eql({
      numerator:
        { value: 7, unit: 'mmol/L', system: undefined, code: undefined },
      denominator:
        { value: 13, unit: 'mmol/L', system: undefined, code: undefined }
    })
  })

  afterEach(() => {
    observation = null
    observationBuilder.setObservation(null)
    observationBuilder = null
  })
})
