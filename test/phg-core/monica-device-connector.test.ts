import { MonicaDeviceConnector } from "../../src/device-measurement-engine"
import { expect } from 'chai'
import { InterfaceEndpoint, MetaData, Baseplate, ModuleBase, MessageType } from 'phg-js-baseplate'
import { BaseplateMock } from "../mocks/BaseplateMock.test"
import { InterfaceEndpointMock } from "../mocks/InterfaceEndpointMock.test"
import { MetaDataMock } from "../mocks/MetaDataMock.test"
import { MeasurementReceivedMock } from "../mocks/MeasurementReceivedMock.test"
import { Device, Observation } from 'fhir'
import { CurrentStatusMock } from "../mocks/CurrentStatusMock.test"


describe('MonicaDeviceConnector', () => {
    let monicaDeviceConnector: MonicaDeviceConnector
    let baseplateMock: BaseplateMock
    let measurementConsumer: InterfaceEndpointMock
    let monicaConsumer: InterfaceEndpointMock
    let events: string[]

    before(() => {
        baseplateMock = new BaseplateMock
        monicaDeviceConnector = new MonicaDeviceConnector(baseplateMock)
        measurementConsumer = <InterfaceEndpointMock>monicaDeviceConnector.getMeasurementConsumer()
        monicaConsumer = <InterfaceEndpointMock>monicaDeviceConnector.getMonicaConsumer()
    })

    it('can return status', () => {
        const string = monicaDeviceConnector.status
        expect(string).to.be.a('string')
        expect(string).to.deep.equal('No statusmessage received')
    })

    it('has implemented FHIRMeasurement consumer', () => {
        expect(measurementConsumer.getModuleTag()).to.equal('FHIRMeasurement')
    })

    it('has implemented Monica consumer', () => {
        expect(monicaConsumer.getModuleTag()).to.equal('Monica')
    })

    it('has added eventhandler for MeasurementReceived', () => {
        expect(measurementConsumer.getEventNames().indexOf('MeasurementReceived')).to.not.equal(-1)
    })

    it('has added eventhandler for CurrentStatus', () => {
        expect(monicaConsumer.getEventNames().indexOf('CurrentStatus')).to.not.equal(-1)
    })

    it('should be able to start measurement', () => {
        var promise: Promise<boolean> = monicaDeviceConnector.startMeasurement()
        promise.then(value => {
            expect(value).to.equal(true)
        })
        expect(monicaConsumer.getPostedEvents().indexOf('Start')).to.not.equal(-1)
    })

    it('should be able to stop measurement', () => {
        var promise: Promise<boolean> = monicaDeviceConnector.stopMeasurement()
        promise.then(value => {
            expect(value).to.equal(true)
        })
        expect(monicaConsumer.getPostedEvents().indexOf('Stop')).to.not.equal(-1)
    })

    it('can perform a MDC585728 measurement', () => {
        var implementation = measurementConsumer.getImplmentation()
        expect(implementation).to.be.a('function')

        var metadata = new MetaDataMock
        var measurementReceived = new MeasurementReceivedMock
        measurementReceived.setDeviceType("MDC585728")
        measurementReceived.setFhirObservation("{\"observation\": 42}")
        measurementReceived.setFhirDevice("{\"device\": 42}")

        implementation(measurementReceived, metadata)
    })

    it('can perform a 585728 measurement', () => {
        var implementation = measurementConsumer.getImplmentation()
        expect(implementation).to.be.a('function')

        var metadata = new MetaDataMock
        var measurementReceived = new MeasurementReceivedMock
        measurementReceived.setDeviceType("585728")
        measurementReceived.setFhirObservation("{\"observation\": 42}")
        measurementReceived.setFhirDevice("{\"device\": 42}")

        implementation(measurementReceived, metadata)
    })

    it('measurement can throw error', () => {
        var implementation = measurementConsumer.getImplmentation()
        expect(implementation).to.be.a('function')

        var metadata = new MetaDataMock
        var measurementReceived = new MeasurementReceivedMock
        measurementReceived.setDeviceType("585728")
        measurementReceived.setFhirObservation(null)
        measurementReceived.setFhirDevice(null)

        expect(() => implementation(measurementReceived, metadata)).to.throw('Malformed json')
    })

    it('can get device and measurement when they are set', () => {
        var implementation = measurementConsumer.getImplmentation()
        var metadata = new MetaDataMock
        var measurementReceived = new MeasurementReceivedMock
        measurementReceived.setDeviceType("585728")
        measurementReceived.setFhirObservation("{\"observation\": 42}")
        measurementReceived.setFhirDevice("{\"device\": 42}")
        implementation(measurementReceived, metadata)

        var promise: Promise<{ device: Device, measurement: Observation }> = monicaDeviceConnector.getDeviceAndMeasurement()
        promise.then(value => {
            expect(value.device).to.deep.equal({ device: 42 })
            expect(value.measurement).to.deep.equal({ observation: 42 })
        })
    })

    it('can receive current status', () => {
        var implementation = monicaConsumer.getImplmentation()
        var metadata = new MetaDataMock
        var currentStatus = new CurrentStatusMock

        var callbackStatus: string

        monicaDeviceConnector.subscribeToStatus(value => {
            callbackStatus = value
        })

        currentStatus.setCurrentStatus('someStatus')
        implementation(currentStatus, metadata)

        expect(monicaDeviceConnector.status).to.be.a('string')
        expect(monicaDeviceConnector.status).to.equal('someStatus')
        expect(callbackStatus).to.equal('someStatus')

    })
})