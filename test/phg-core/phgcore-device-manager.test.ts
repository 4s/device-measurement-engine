import { PHGCoreDeviceManager } from "../../src/device-measurement-engine"
import { expect } from 'chai'
import { initialize, baseplate } from "phg-js-baseplate"

describe('DeviceManger', () => {
    let deviceManger: PHGCoreDeviceManager

    beforeEach(() => {
        initialize(function (buf: any) { })
        deviceManger = new PHGCoreDeviceManager(baseplate)
    })

    it('predicate removes duplicates', () => {
        const codes = ["a", "b", "a", "c", "d", "b", "d"]
        const newCodes = codes.filter(deviceManger.removeDuplicatesPredicate())
        expect(newCodes.length).to.equal(4)
        expect(newCodes).to.contain("a")
        expect(newCodes).to.contain("b")
        expect(newCodes).to.contain("c")
        expect(newCodes).to.contain("d")
    })

    it('search can return test', () => {
        deviceManger.searchForDevices(['test']).then(devices => {
            expect(devices.length).to.equal(1)
        })
    })

    it('search can return something on input 585728', () => {
        deviceManger.searchForDevices(['585728']).then(devices => {
            expect(devices.length).to.equal(1)
        })
    })

    it('search can return something on input 8450059', () => {
        deviceManger.searchForDevices(['8450059']).then(devices => {
            expect(devices.length).to.equal(1)
        })
    })

    it('search can return something on input mdc8450059', () => {
        deviceManger.searchForDevices(['mdc8450059']).then(devices => {
            expect(devices.length).to.equal(1)
        })
    })

    it('search can return something by default', () => {
        deviceManger.searchForDevices(['abcdefg']).then(devices => {
            expect(devices.length).to.equal(1)
        })
    })
})