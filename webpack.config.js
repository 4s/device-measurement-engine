const path = require('path');
module.exports = {
    entry: './src/device-measurement-engine.ts',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    output: {
        library: 'device-measurement-engine',
        libraryTarget: 'umd',
        filename: 'device-measurement-engine.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: "source-map"
};